# Step 2: Generate Characteristics
Characteristics are the primary building blocks of the character.
All characters and creatures have five characteristics, which give the basic information about the character's capabilities.
As well as being useful indicators of how to roleplay the character (see below) they are the scores that skills are initially based upon. 

## Characteristics
+ _Body_ (**BOD**):
  > A character's overall physical health, stature, and strength.
+ _Dexterity_ (**DEX**):
  > A character's agility, co-ordination and speed of reaction.
+ _Reason_ (**REA**):
  > A character's ability to think around problems, analyse information and memorise instructions.
+ _Intuition_ (**INT**):
  > This quantifies the accuracy and frequency of a character's 'gut feelings' and senses.
+ _Charisma_ (**CHA**):
  > This quantifies a character's life force, presence, and the strength of their willpower.

## Generation
Each characteristic has a starting value of $$5$$ and a maximum value of $$18$$.
You next have $$40$$ points to distribute amongst them.
A characteristic of 10 is considered "average".

> **[success] Example: Kost's Characteristics**
>
> Kost, a philosopher-turned-battle-Crafter, begins play with the following characteristics:
>
> **BOD** $$9$$ **DEX** $$10$$ **REA** $$15$$ **INT** $$13$$ **CHA** $$18$$
>
> Kost spent 4 points on his body, 5 points on his dexterity, 13 points on reason and charisma, and 5 points on his intuition.
> Kost is roughly average when it comes to his physical characteristics, but he is a hugely powerful person whose mind is sharp and intuitive.

## Tests
Characteristics provide your basic chance to succeed at a task in the game.
Tests come in two categories - active and reactive.
Every test will always include a characteristic as the base and most tests will gain a skill bonus.
Your [intent and approach](../2-tests-and-skills/README.md) will inform both the appropriate characteristic and skill, though the GM will decide the matter where ambiguous.
In cases where more than one skill applies to the test you should use the one with the best score.

**Active** tests are called for when your character is attempting a task of their own volition and initiative.
Examples of active tests include attacking an enemy, persuading a merchant, climbing a cliff, searching a room, and researching lore in a library.

**Reactive** tests are called for whenever you need to know if your character performs a task in response to external events.
Examples of reactive tess include parrying an enemy, resisting the charms of a merchant, catching a handhold while falling down a cliff face, noticing someone sneaking up behind you, and knowing a particular piece of heraldry on sight.

Your character's score for an active test is $$2\times$$ the appropriate characteristic.

Your character's score for a reactive test is $$3\times$$ the appropriate characteristic.

To make a test you will roll $$1\text{D}100$$.
If the result of this roll is **less than** the total of your test score and applicable skill bonus, you succeed.

For more information, see [CH2](../2-tests-and-skills/README.md).

> **[success] Example: Kost's Base Test Scores**
>
> | Characteristic | Score  | Active   | Passive  |
|:----------------:|:------:|:--------:|:--------:|
| **BOD**          | $$9$$  | $$18\%$$ | $$27\%$$ |
| **DEX**          | $$10$$ | $$20\%$$ | $$30\%$$ |
| **REA**          | $$15$$ | $$30\%$$ | $$45\%$$ |
| **INT**          | $$13$$ | $$26\%$$ | $$39\%$$ |
| **CHA**          | $$18$$ | $$36\%$$ | $$54\%$$ |