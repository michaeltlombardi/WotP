# Step 4: Calculating Skills
Skills are things that the character has learned to do.
They represent very specific areas of expertise and when the skill is used in play the score of the skill is added to a test.
Skills are a bonus on top of the base percentage chance for success determined by the [characteristic](2-characteristics.md#tests).

> **[info] What do the Numbers Mean?**
>
> This table translates the skill score into a 'real-world' description of the level of expertise.
>
| Skill       | Expertise  | Description                                                                                                 |
|:-----------:|:----------:|:------------------------------------------------------------------------------------------------------------|
| $$0-25\%$$  | Novice     | Someone with virtually no experience with the skill                                                         |
| $$26-50\%$$ | Apprentice | Somone who is learning the skill                                                                            |
| $$51-75\%$$ | Veteran    | Someone who regularly uses the skill as part of their everyday life                                         |
| $$76-99\%$$ | Expert     | A local expert within the skill who can perform it under most conditions with ease and elegance             |
| $$100+$$    | Master     | The acknowledged best in that skill who regularly performs the impossible in extremely difficult conditions |

The following tables include a **non-exhaustive** list of skills that characters may possesses.
The tables also give each skill a brief description of what the skill does, but a fuller description is given in [CH2](../2-tests-and-skills/README.md).

Skills are grouped into categories for easy reference:
+ Resistances are skills that get the character out of harms way. 
+ Combat skills measure the characters skills in physical violence. 
+ Knowledge skills measure the intellectual skills of the character. 
+ Practical skills measure the characters' ability of performing a variety of everyday and specialist tasks. 
+ Magic skills cover either casting magic or knowledge of religious practices. 

> **[info] A note on skills with descriptors in brackets.**
>
> Lore (Type), and Culture (other) all have a descriptor in brackets after the main skill name.
> This denotes that at time of points allocation the player must decide what ‘type' or ‘other' is.

## Resistance Skills
Spend 50 points between the following skills:

| Skill       | Description |
|:-----------:|:------------|
| Dodge       | Gets you out of the way of physical threats, such as an incoming axe or dragon breath 
| Persistence | Is a measure of your character's willpower and resistance against hostile magic and is also rolled to resist attempts to influence the character against their will.
| Resilience  | Is a measure of your character's health and their ability to survive exposure, hunger and thirst. It is also their chance to resist the effects of diseases

## Combat Skills
Spend 50 points between the following skills:

| Skill          | Description |
|:--------------:|:------------|
| Close Combat   | A character's skill in melee with weapons, punches, kicks, grappling, etc.
| Ranged Combat  | A character's skill with missile and thrown weapons

## Knowledge Skills
Spend 50 points between the following skills:

| Skill            | Description |
|:----------------:|:------------|
| Culture (Own)    | What a character knows about the history, politics, geography of their own land and society 
| Language (Own)   | How fluent a character is at speaking and potentially reading and writing their own language
| Natural Lore     | Predicting the weather, recognizing and caring for animals and plants, geology and survival in the natural world
| Culture (Other)  | What a character knows about the history, politics and geography of a foreign land
| Language (Other) | How fluent a character is at speaking and potentially reading and writing a foreign language
| Lore (Type)      | Other specialist knowledge skills, such as Lore (Herbalist), Lore (Heraldry) not covered by the above

## Practical Skills
Spend 75 points between the following skills:

| Skill               | Description |
|:-------------------:|:------------|
| Athletics           | Measures the character's ability at running, lifting, jumping, swimming and climbing
| Artisanry (Type)    | Allows you to make things, such as pots, weapons, and buildings
| Deception           | Stealth, hiding and picking pockets are all handled by this skill
| Driving             | This skill covers ancient-medieval period vehicles such as carts, chariots and wagons
| Handling            | Used when trying tricky manoeuvres using beasts of all sorts
| Healing             | Healing wounds and treating disease using First aid and surgery
| Influence           | Used when the character wants to persuade another to do something against their normal interests
| Mechanisms          | Locks and anything with complex moving parts are covered by this skill
| Perception          | Used to spot hidden objects and spot small details in the character's environment
| Performance (Type)  | Acting, playing instruments, dancing and singing are all covered by this skill
| Relationship (Type) | Shows the strengh of a character's bond with a non-player character or group of non-player characters
| Sailing             | Covers the use of ships and boats
| Streetwise          | Finding out information, navigating around the streets, finding a fence for stolen goods, etc.
| Trade               | Used by merchants and traders to value and sell goods
| Wealth              | This skill shows the resources and physical possessions a character has access to.

## Magic Skills
These skills are special, refer to [step 5](5-Magic.md).

| Skill         | Description |
|:-------------:|:------------|
| Crafting      | Used to exercuse Crafting Knacks, cast and manipulate spells, and use magic items
| Knackery      | Used to exercise Personal Knacks
| Supplication  | Used to exercise Supplication Knacks and advance within an organization's hierarchy

> **[info] Innate and Trained Magic**
>
> All characters in Tephra have knackery and _some_ skill in the Craft.
> Not all characters who can cast spells can do so in combat; combat crafting is always difficult, and thus makes the test _[hard](../2-tests-and-skills/README.md)_.
> So, while lots of characters will be capable of casting spells, _especially_ ones they know well, few will be able to do so reliably (or safely!) in combat.
> The Crafting skill also indicates a character's general awareness of magic, not necessarily their abilities as a battlemage.