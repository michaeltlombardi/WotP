# Step 5: Figure Out Magic
Unlike tests for nearly every other intent and approach, magic always relies on the same characteristics: Knackery and Supplication on Charisma and Crafting on Reason.

## Knacks and Knackery
The world of Tephra is _super-saturated_ with magic; so much so that _every_ character has inbuilt semi-magical abilities, representative of their gestalt.
That is, in this world, people _become_ what they do and devoutly believe.
Magic saturates every person and causes them to change slowly over time - people with strong convictions who exercise them change faster and more obviously.
The abilities derived from these changes and this magical influence are referred to as knacks.

Every time a character uses a knack they must spend Power Points equal to the knack's power, or _Magnitude_.
The maximum Magnitude of any knack a character knows is bounded by their Knackery skill.
That is, characters cannot have any knack with a Magnitude greater than $$\frac{1}{10}$$ their Knackery skill score.

If a character has a knack with a Magnitude greater than one they can choose to exercise it at a lower magnitude.
Knacks exercised at a lower Magnitude cost less.

Every character starts play with a knackery score of $$\text{CHA} \times 3$$ and six points of Magnitude worth of Knacks.
Characters who do not start play as Supplicants or Crafters (see below) starts play with twelve improvement points they can spend immediately.

## The Craft and Crafting
In Tephra, on the Sweetwater and in Breccia, almost everyone knows _something_ of the Craft, the manipulation of reality by applying the natural laws of magic and research into those laws; therefore, all characters begin play with a Crafting score equal to their REA.

If a player wants their character to start play as a Crafter (one for whom the Craft _is_ their profession, as opposed to someone who just knows some of the Craft or uses it for their mundane profession), they must choose a School to study at, a Master to apprentice to, or be a Hedgecrafter (see [CH7](../7-the-craft/README.md) for more details).

In any case, increase the character's starting Crafting skill by $$30$$ and choose six points worth of spells (one point per spell) and/or knacks (one point per magnitude).
Knacks gained from studying the Craft (remember, characters change because of their actions and beliefs) use your Crafting score instead of Knackery to determine their maximum magnitude.

## Supplication
In addition to the personal knacks that inhabitants of Tephra develop and manipulation of magic via the Craft, characers may also be supplicants of a faction.
Supplication implies dedication to a particular organization - whether that be a cult, a guild, a kingdom, a government, or other power.
Supplication grants the supplicant access to faction-specific knacks and empowers them to perform miracles on behalf of their faction.
Not all followers or members of a faction are Supplicants; only those who desire to wield power on behalf of the faction and to further the faction's goals become Supplicants, and only then after winning the favor and trust of the faction.
The duties and magical contract binding a Supplicant are such that while someone can belong to more than one faction they can only be a Supplicant of a single faction at a time.

If a player wants their character to start play as a Supplicant, they must choose a faction to join.
The character's starting Supplication skill for that faction is $$\text{CHA} \times  3$$.
Further, the supplicant can choose up to six points worth of supplication knacks, specified by their faction, and/or miracles with one point per magnitude (to a maximum of 2).

Read [CH6](../6-supplication/README.md) for more information.