# Step 1: Determine Concept
In one sentence sum up what your character is all about.
Ask the other Players what their character concepts are to make sure the group has an interesting selection of characters. 
Check with your Games Master that your character concept fits in with the type of game that the group is going to be playing. 

> [success] Examples:
>
> + **Rurik** is _'A determined and foolhardy warrior seeking excitement and adventure.'_
> + **Lura** is _'A mysterious and elegant sorceress. '_
> + **Mancala** is _'The illegitimate son of a murdered Noble, who survives through being a rogue.'_
> + **Abnon** is _'A pious priest who smites evil and protects the innocent.'_