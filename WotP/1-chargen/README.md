# Character Generation

## What is a Player Character?
A character is your representation in the game.
Your eyes, ears, touch, feel and smell in the imaginary world that you and your fellow players create. 

On one hand the character is a collection of numbers which describe his/her characteristics, skills and magic spells that are written down on a character sheet.
This chapter will explain how you create these numbers, in a process known as ‘Character Generation'. 

_But that's only half of what a character is._

The other half exists mainly in the imagination of the player, with perhaps some quick notes on the character sheet.
This half is the personality of the character and other intangibles such as goals and past history.
These are the things that you can't express in cold hard numbers, which really bring the character to life and give the player guidelines on how the character acts and thinks. 

## Character Generation
The process of creating a character is known as ‘Character Generation'.
Character generation in WotP is a seven step process and at each step the Player makes decisions about what their character is like at the beginning of the game, when the character is just starting out on their adventuring career.

1. [Determine Concept](./1-Character-Concept.md)
2. [Generate Characteristics](./2-Characteristics.md)
3. [Determine Attributes](./3-Attributes.md)
4. [Calculate Skills](./4-Skills.md)
5. [Figure out Magic](./5-Magic.md)
6. [Get Equipment](./6-Equipment.md)
7. [Finish off](./7-Finishing.md)