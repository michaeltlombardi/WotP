# Step 3: Determine Attributes
Attributes are a set of secondary scores that define the character's potential to do and take physical damage, how quickly they move, and the amount of magical energy available to the character.
Attributes are determined from the character's characteristic scores - _hit points_ from body and charisma, _power points_ from charisma, and _movement rate_ from dexterity.

## Damage Bonus (**DB**)
In combat, characters and creatures can add $$\frac{1}{4}\text{BOD}$$ to their melee attacks.
Optionally, characters can add $$\frac{1}{4}\text{DEX}$$ to ranged attacks and melee attacks with one-handed weapons.

## Hit Points (**HP**)
These determine how much damage the character can sustain before reaching unconsciousness or death and has a starting value of $$\frac{BOD + CHA}{2}$$.

> **[info] Rounding:**
> 
> Numbers in WotP are always rounded to the nearest whole number.
> Always round up from $$0.5$$ and round down from below $$0.5$$.
> For example $$4.1$$ becomes $$4$$, while $$5.5$$ becomes $$6$$.

When a character takes damage equal to half their HP in one go, they suffer a _Major Wound_.
_Major Wounds_ are injuries, such as a severed limb, broken ribcage, or broken skull, which may render the character unconscious immediately and leave permanent affects if not properly healed.

## Power Points (**PP**): 
Power points are used to activate any knacks the character knows and has a starting value equal to the character's **CHA**.
They are tracked separately from **CHA** because they fluctuate over the course of a day.

## Movement Rate (**MR**):
A character's movement rate determines how many yards they can move in a five second Combat round and has a starting value equal to the character's **DEX**.
A character's movement rate is tracked separately from **DEX** because it can be modified by injuries, armor, and other factors.