# Combat Reference Tables

## Combat Actions
| Action               | Effect |
|:---------------------|:-------|
| Charge               | Character moves twice movement, followed by a close combat attack with a $$+1\text{D}6$$ to damage. Loses Reaction for the round.  
| Close Combat Attack  | Character attacks opponent with weapon, tests vs. Close Combat skill. If successful does weapon damage plus damage modifier. 
| _All Out Attack_     | Two attacks at $$-25\%$$. Gives up Reaction for round. 
| _Great Attack_       | One attack at $$+25\%$$ at maximum damage. Gives up Reaction for round.
| _Disarming Attack_   | Attack at $$-25\%$$ to disarm opponent
| _Grapple_            | 
| Set Weapon           | Character sets spear or polearm in anticipation of charge. When charge occurs character attacks first with $$+25\%$$ to weapon skill
| Standard Move        | The character moves their Movement Rate in yards as a free action, once per round. 
| Change Stance        | May move from prone to standing and vice versa. 
| Fighting Retreat     | Moves their Movement Rate and defends at $$+25\%$$ ($$+50\%$$ if using a Medium or Large Shield). 
| Sprint               | Moves twice Movement Rate. May not attack and may only Dodge as a reaction
| Ranged Combat Attack | Character attacks opponent with weapon, tests vs. Ranged Combat skill if successful then does weapon damage plus damage modifier.
| _Aim_                | Spend a round aiming to gain a $$+25\%$$ bonus to hit the target.
| Cast Spell           | The spell takes effect in initiative order but is not cast if casting interrupted and concentration lost.
| Intimidate/Persuade  | The character uses their Influence skill vs the enemies' Persistence to either intimidate, or persuade foes who are facing defeat, to flee or surrender
| Skill Use            | Character uses a non combat skill

## Combat Results

| Attacker   | Defender's Reaction | Result |
|:----------:|:-------------------:|:------:|
| Fumble     | No need to roll     | Attacker fumbles.
| Failure    | No need to roll     | Attacker fails to hit defender.
| Success    | Fumble              | Attacker hits, defender takes damage rolled minus AP and fumbles.
| Success    | Failure             | Attacker hits, defender takes damage rolled minus AP.
| Success    | Success             | If dodging, defender avoids; if parrying, damage reduced based on size.
| Success    | Critical            | Defender avoids attack and takes no damage; ignore size for parrying weapon/shield.
| Critical   | Fumble              | Attacker does maximum damage and ignores defender's AP. Defender fumbles.
| Critical   | Failure             | Attacker does maximum damage and ignores defender's AP.
| Critical   | Success             | Attacker does maximum damage and ignores defender's AP.
| Critical   | Critical            | Attacker hits, defender takes damage rolled minus AP.

## Close Combat Situational Modifiers

| Situation                                                 | Skill Modifier |
|:---------------------------------------------------------:|:--------------:|
| Target is helpless                                        | Automatic Crit |
| Target is prone or attacked from behind                   | $$+25\%$$      |
| Attacking or defending while on higher ground or on mount | $$+25\%$$      |
| Attacking or defending while prone                        | $$-25\%$$      |
| Attacking or defending while on unstable ground           | $$-25\%$$      |
| Attacking or defending while underwater                   | $$-50\%$$      |
| Defending while on lower ground or against mounted foe    | $$-25\%$$      |
| Fighting in partial darkness                              | $$-25\%$$      |
| Fighting in darkness                                      | $$-50\%$$      |