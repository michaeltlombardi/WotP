# Miscellaneous Rules

## Mounted Combat
A mounted character's attacks and parries against adjacent opponents on foot are easy tests and defensive reactions by characters on foot against mounted characters are hard tests.

A mounted character uses their mount's Movement Rate when moving instead of their own.

A mounted character's close combat and ranged combat skills are constrained by their riding skill.
That is, if a character has a ranged combat skill of $$60\%$$ and a riding skill of $$40\%$$ then, while mounted, their ranged combat skill only counts as $$40\%$$.

## Two Weapon Use
A character wielding two weapons or a weapon and a shield may use the off-hand item to _either_:

+ Parry one additional attack per Combat Round (over and above the normal Reaction allowance)
+ Gain a single bonus close Combat Attack action at $$-25\%$$ Close Combat Skill.
  This may only be a normal close combat attack, not a special close combat attack.

## Splitting Skills
A character can split their skill to perform multiple attacks, parries, or dodges.

The number of attacks and the allocated skill $$\%$$ must be declared as normal.
Any allocation of split is allowed.

> **[success] Example of Splitting Skills**
>
> Morgan the Mighty has a Close Combat skill of $$80\%$$.
> She can choose split her skill and make two attacks at $$40\%$$ each, or one at $$50\%$$ and the other at $$30\%$$, or four attacks at $$20\%$$, or whatever other split she wants.

Similarly, a character can choose to split their dodge or parry reaction by skill, _but must choose to do so during action declaration_ in initiative order.

## Cover
Cover affects both ranged and close combat attacks.
For missile attacks the defender benefits from the best of the following cover modifiers:

+ Partial cover ($$-25\%$$): A low wall that leaves only head and torso exposed, or wielding a medium shield.
+ Very good cover ($$-50\%$$): On a castle wall, firing from protected battlements, or wielding a large shield.
+ Virtually total cover ($$-75\%$$):Castle wall with arrow slits for defenders to shoot through, or wielding a huge shield.