# Combat
It should be remembered that Winter of the Phoenix is not a game purely about combat, just as it is not purely about magic.
It would not be unusual for whole sessions to pass without any physical violence.
However, in time, characters will get involved in dangerous life threatening fights.

This chapter provides you with a straightforward and direct system for playing out action packed and deadly combat.

## Basics
+ A Combat Round lasts five seconds.
+ At the beginning of each round, all characters involved roll for initiative.
  The characters declared intended actions _and_ reactions from highest to lowest roll.
  The actions then take place _in reverse order of declaration_ - the character with the lowest initiative roll declares their action last but takes effect first.
  In cases where there is a tie, the character with the lower initiative skill declares first.
  If the skills are somehow also a tie, the character with the lower DEX declares first.
  If their DEX scores are also tied, flip a coin.
+ You get one Combat Action, usually an attack, and one Reaction, usually a defensive action, per combat round.
  You can take additional actions at a cumulative $$-25\%$$ penalty to all actions attempted during the Combat Round.
+ You can move your Movement Rate in a Combat Round without losing your Action or Reaction.
+ You can run twice your Movement Rate in a Combat Round but you may only Dodge as your Reaction.
+ To defend or attack you roll against your Close Combat or Ranged Combat skill depending on the type of weapon you are using.
+ When attacked you can either Parry (use the Close Combat skill) or Dodge as a Reaction.
+ If your character successfully Dodges an attack they take no damage.
+ If your opponent successfully Parries your attack their weapon or shield reduces the damage your attack does.
+ If you successfully hit your opponent takes damage to their hit points equal to $$
    \text{Weapon damage rolled} + \text{your Damage Bonus} - (\text{Opponent's Armour Points})
  $$

## Summary of Combat
+ **Work out encounter distance:** The GM determines how far away the hostile group is to the player characters, either at Range or Close.
+ **Drop into Combat time:** Combat is divided into rounds. A single round has a duration of five seconds of time, giving 12 rounds in every minute. During a round every character can perform one action. Combat rounds cycle through the following steps:
+ **Determine order:** At the start of every round of combat, all characters make an initiative roll.
+ **Declare Action & Reaction:** In a combat round each character gets one Combat Action and one Defensive Reaction.
  Characters declare their action and their reaction in order from highest to lowest initiative roll.
+ **Resolve Action & Reaction:** In order from lowest to highest initiative roll all of the declared actions and reactions are resolved.
  Note that while reactions are declared at the same time as actions they are resolved at the time of the triggering attack or action.
  Sometimes, declared reactions will not be useful (because the attacker, with a lower initiative, decided not to press the attack for example).
+ **For example:** Lura (INI $$56$$, roll $$32$$) is casting a spell, while a Goblin (INI $$36$$, roll 42$$) is attacking her with a sword and Rurik (INI $$64$$, roll $$12$$) also attacks the Goblin with his sword. The order of action is Lura first, then the Goblin and finally Rurik. FIX ME.
+ **End of Combat Round:** Once all eligible characters have acted in the combat round, it is over.
  If there are characters still engaged in combat with enemies, another combat round begins.

> **[info] Encounter Distance and Engaging in Combat**
>
> Not all combats start with the two sides, the players and their opponents, directly facing each other within swords reach.
> At the beginning of a combat, or potential combat, the GM must determine which of the two distances the encounter starts at. 
>
> **Close** is a range of two yards or less and is the distance at which a character can engage in Close combat.  
>
> **Ranged**, beyond two yards up to double the range of the missile weapon a character is holding, is the distance at which the character can engage in ranged combat.
> Ranged combat typically happens out in the open countryside where groups of combatants can see each over coming over the horizon or emerging in the distance from old ruined buildings.

