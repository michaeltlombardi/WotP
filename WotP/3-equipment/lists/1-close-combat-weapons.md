# Close Combat Weapons
All Close Combat weapons use the Close Combat skill.
Each close combat weapon is characterised by the following qualities:

+ **Damage Dice:** The damage the weapon deals on a successful attack.
+ **ENC:** The weapon's Encumbrance. The weight and bulk of the weapon.
+ **Size:** Weapons are rated in the following size categories: Small, Medium, Large and Huge.
  Weapons need to be of the same category or larger to block all damage.
  If the defending weapon is one category less they block half damage.
  If two categories less they cannot block the damage.
+ **Cost:** The cost in silver pieces to purchase this weapon.

> **[warning] Close Combat Weapons**
>
> | Weapon                                                         | Damage Dice | ENC | Size   | Cost |
|:----------------------------------------------------------------:|:-----------:|:---:|:------:|:----:|
| **Melee Weapons**                                                |||||
| Unarmed                                                          | $$1\text{D}4$$     | -   | Small  | -      |
| Club (flex), Hatchet (Range)                                     | $$1\text{D}6$$     | 1   | Small  | 20 SP  |
| Quarterstaff (2H)                                                | $$1\text{D}8$$     | 2   | Medium | 20 SP  |
| Battleaxe (flex), Warhammer (flex), War Pick (flex), Mace (flex) | $$1\text{D}8$$     | 2   | Medium | 120 SP |
| Greathammer (2H), Greataxe (2H), War Maul (2H)                   | $$2\text{D}8$$     | 4   | Large  | 200 SP |
| Halberd (LS, 2H), Polearm (LS, 2H), Glaive (LS, 2H)              | $$1\text{D}8$$     | 3   | Large  | 200 SP |
| **Shields**                                                      |||||
| Small [Buckler, psilos, etc]                                     | $$1\text{D}4$$     | 1   | Small  | 50 SP  |
| Medium [Target shield]                                           | $$1\text{D}6$$     | 2   | Medium | 150 SP |
| Large [Hoplon, kite, etc]                                        | $$1\text{D}6$$     | 3   | Large  | 300 SP |
| Huge [Pavise]                                                    | $$1\text{D}8$$     | 4   | Huge   | 450 SP |
| **Spears** (Set, Flex)                                           |||||
| Lance                                                            | $$1\text{D}10$$    | 3   | Large  | 150 SP |
| Longspear (LS, 2H)                                               | $$1\text{D}8$$     | 2   | Medium | 30 SP  |
| Shortspear (Range)                                               | $$1\text{D}6$$     | 2   | Medium | 20 SP  |
| **Swords and Knives**                                            |||||
| Dagger (Range)                                                   | $$1\text{D}4 + 1$$ | -   | Small  | 20 SP  |
| Khopesh (flex), Kopis                                            | $$1\text{D}6$$     | 2   | Medium | 150 SP |
| Rapier                                                           | $$1\text{D}6$$     | 1   | Light  | 150 SP |
| Shortsword                                                       | $$1\text{D}6$$     | 1   | Medium | 100 SP |

## Notes

+ **Set:** This weapon may be set against a charge.
+ **Range:** This weapon suffers no penalty when thrown.
+ **LS:**  This weapon may be used as a Longspear.
  If used as a Longspear it may be set against charges.
  The wielder must state, however, at the start of combat how it is being wielded and must take a ‘Change stance' action to alter its usage.
+ **Flex:** This weapon can be used two-handed.
  When used in two hands, it gains +1 damage.
+ **2H:** This weapon must be used two-handed.
+ **Improvised and primitive weapons:** such as a stone hatchet, stone spear or a convenient log picked up and used as a club, do the same damage as the base weapon -1.