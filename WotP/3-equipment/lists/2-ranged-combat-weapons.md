# Ranged Weapons

Each ranged weapon is characterised by the following qualities:

+ **Damage Dice:** The damage the weapon deals on a successful attack.
+ **Range:** This is the effective range of the weapon.
  A target within the weapon's range may be attacked without penalty.
  A target within double the weapon's range  is a hard test.
  Attacks against targets beyond double the weapon's range automatically fail.
+ **Rate:** This shows how many missiles can be fired per Combat Rounds, taking into consideration the time to load the weapon.
  For example 1 CR, means 1 missile can be fired every combat round, while 1/3 CR means one 1 missile every 3 combat rounds.
+ **ENC:** The weapon's Encumbrance.
  The weight and bulk of the weapon.
+ **Cost:** The cost in silver pieces to purchase this weapon.

> **[info] Using Ranged Weapons in Close Combat**
>
> If used in close combat, a ranged weapon is treated as an improvised weapon, doing damage equal to its closest hand-to-hand equivalent if that is less than its ranged weapon damage.

<!-- Comment Break to allow adjacent alert blocks-->

> **[warning] Ranged Weapon Ammunition**
>
> | Ammunition       | ENC | Cost |
|:------------------:|:---:|:----:|
| Arrows (10)        | -   | 1 SP |
| Bolts (10)         | -   | 2 SP |
| Sling Bullets (10) | -   | 5 CP |

<!-- Comment Break to allow adjacent alert blocks-->

> **[warning] Ranged Weapons**
>
> | Weapon                         | Damage Dice        | Range                     | Rate                        | ENC   | Cost              |
|:--------------------------------:|:------------------:|:-------------------------:|:---------------------------:|:-----:|:-----------------:|
| **Missile Weapons**              ||||||
| Heavy Crossbow (2H)              | $$2\text{D}6$$     | $$150\text{yd}$$          | $$ \frac{1}{3} \text{CR} $$ | $$2$$ | $$350 \text{SP}$$ |
| Light Crossbow (2H)              | $$1\text{D}8$$     | $$125\text{yd}$$          | $$ \frac{1}{2} \text{CR} $$ | $$1$$ | $$150 \text{SP}$$ |
| Long Bow (2H)                    | $$1\text{D}10$$    | $$150\text{yd}$$          | $$ 1 \text{CR} $$           | $$1$$ | $$150 \text{SP}$$ |
| Short Bow (2H), Staff Sling (2H) | $$1\text{D}8$$     | $$125\text{yd}$$          | $$ 1 \text{CR} $$           | $$1$$ | $$125 \text{SP}$$ |
| Sling (1H)                       | $$1\text{D}6$$     | $$150\text{yd}$$          | $$ 1 \text{CR} $$           | $$-$$ | $$5 \text{SP}$$   |
| **Thrown Weapons**               ||||||
| Atlatl (2H)                      | $$+2$$             | $$BOD \times \text{yd}$$  | $$ \frac{1}{2} \text{CR} $$ | $$1$$ | $$20 \text{SP}$$  |
| Dagger (Close)                   | $$1\text{D}6$$     | $$BOD \times \text{yd}$$  | $$ 1 \text{CR} $$           | $$-$$ | $$30 \text{SP}$$  |
| Hatchet (Close)                  | $$1\text{D}8$$     | $$BOD \times \text{yd}$$  | $$ 1 \text{CR} $$           | $$1$$ | $$25 \text{SP}$$  |
| Dart                             | $$1\text{D}4$$     | $$BOD \times \text{yd}$$  | $$ 1 \text{CR} $$           | $$-$$ | $$5 \text{SP}$$   |
| Javelin / Short Spear (Close)    | $$1\text{D}6$$     | $$BOD \times 2\text{yd}$$ | $$ 1 \text{CR} $$           | $$1$$ | $$20 \text{SP}$$  |
| Rock / Improvised                | $$1\text{D}4$$     | $$BOD \times m\text{yd}$$ | $$ 1 \text{CR} $$           | $$1$$ | $$-$$             |

## Notes
+ **1H:** This weapon is a one-handed weapon. 
+ **2H:** This weapon must have two hands free to be used effectively unless otherwise specified.
+ **Close:** This weapon suffers no penalty when used in Close Combat. 