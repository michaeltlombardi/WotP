# Armor
Each piece of armor is characterised by the following qualities:  

+ **AP:** How many armor points this type of armor provides.  
+ **ENC:** The armour's Encumbrance.
  The weight and bulk of the armor.  
+ **Cost:** The cost in silver pieces to purchase this armor.  

> **[warning] Armor**
>
> | Armor   | AP | ENC | Cost    | Description |
|:---------:|:--:|:---:|:-------:|:------------|
| Padded    | 2  | 1   | 150 SP   | Armor made from quilted material, principally using numerous layers to resist damage; cheap, flexible, and readily available
| Linen     | 4  | 3   | 500 SP  | Layers of linen glued and shaped into stiff segments; cheap and readily available
| Chainmail | 6  | 5   | 3000 SP | Links of chain made into a suit; expensive but relatively more maintainable than scale or plate
| Scalemail | 6  | 8   | 1500 SP | Metal scales sewn onto leather backing; cheaper than chain but more encumbering
| Platemail | 8  | 8   | 9000 SP | Bronze plates that cover the body over a chainmail backing


## Notes
+ **Plate Armor:** Characters may try using plate armor not designed for them but the ENC will be doubled.  
+ **Layering Armor:** Characters may not wear more than one type of armor, i.e. layer armor, to get increased Armor Points.  