# The Equipment Lists

The rest of this chapter is given over to equipment lists.
These lists provide the cost of the item and details any game effects.
They also where appropriate give an Encumbrance value (ENC) for the item in question.
This is a value which rates both the weight and how physically unwieldy an item is.
This is for the Encumbrance rules given in [CH8](../8-Questing/README.md). 