# Miscellaneous Items

## General Items

+ **Backpack:** It can hold 20 ENC of equipment.
+ **Block & Tackle:** Adds +25% to Mechanisms tests to make or disarm large traps and makes Engineering tests possible in some circumstances.
  It requires at least $$10\text{yd}$$ of rope to function.
+ **Candle, 1 Hour:**  A candle illuminates a one yards radius.
  Any wind stronger than a slight breeze will extinguish a candle.
+ **Climbing Kit:**  A climbing kit provides a bonus of +25% to any Athletics skill tests made to climb.
+ **Crowbar:** Adds +25% to brute force Athletics tests.
  If used as a weapon, it is considered a club (wielded with a –25% penalty).
+ **First Aid Kit:** A first aid kit is good for five uses (whether the skill test succeeds or fails).
+ **Fish Hook:** This item allows a character to use his Lore (Natural World) skill to catch a fish without suffering a penalty on the test.
+ **Fishing Kit:** The fishing kit grants a character a +25% bonus to his Lore (Natural World) test to catch fish.
+ **Flint & Tinder:** A character with flint and tinder can build a fire in one minute under normal conditions without having to roll his Lore (Natural World) skill.
+ **Grappling Hook:** It will support the weight of 50 ENC.
+ **Hammer:** If used as a weapon, it is treated as a club (wielded with a –25% penalty).
+ **Lantern:** A lantern provides clear illumination out to a three yards radius.
  It will burn for two hours on a flask of oil.
+ **Mining Pick:** If used as a weapon, it is considered a club (wielded with a –25% penalty).
+ **Oil, Flask:** A flask of oil is enough to fuel a lantern for two hours or, if broken on the ground and ignited, enough to sustain a small fire for one minute.
+ **Quiver:** Quivers can hold up to 30 arrows or crossbow bolts.
+ **Rope, 10 Yards:** A standard rope can support the weight of 50 ENC
+ **Sack, Large:** Able to hold 10 ENC of equipment.
+ **Sack, Small:** A small sack can hold 5 ENC of equipment.
+ **Scythe:** If used as a weapon, it is considered a polearm (wielded with a –25% penalty).
+ **Slingbag:** It can carry 15 ENC of equipment.
+ **Spade:** If used as a weapon, it is considered a club (wielded with a –25% penalty).
+ **Waterskin:** A waterskin can hold enough water to sustain an adventurer for two days.

## Animals and Transportation
| Animal                 | Cost           |
|:----------------------:|:--------------:|
| Bison                  | 200 SP         |
| Bull                   | 250 SP         |
| Cart                   | 75 SP          |
| Cat                    | 2 SP           |
| Chariot                | 600 SP         |
| Cow                    | 150 SP         |
| Dog, Domestic          | 2 SP           |
| Dog, Hunting           | 25 SP          |
| Fowl                   | 1 SP           |
| Goat                   | 50 SP          |
| Hawk                   | 400 SP         |
| Horse, Draft           | 400 SP         |
| Horse, Riding          | 350 SP         |
| Horse, Combat Trained  | 500 SP         |
| Mule                   | 125 SP         |
| Ox                     | 200 SP         |
| Pig                    | 50 SP          |
| Rhino                  | 3000 SP        |
| Saddle & Bridle        | 75 SP          |
| Sheep                  | 30 SP          |
| Travel (by Coach)      | 10 SP per Mile |
| Travel (by Post-Horse) | 4 SP per Mile  |
| Travel (by Ship)       | 2 SP per Mile  |
| Travel (by Wagon)      | 2 SP per Mile  |
| Wagon                  | 300 SP         |
| Zebra                  | 300 SP         |

## Food and Lodging
| Item                            | Cost |
|:-------------------------------:|:----:|
| Lodging, Poor                   | 2 CP |
| Lodging, Average                | 1 SP |
| Lodging, Superior               | 5 SP |
| Food and Drink, Poor, 1 Day     | 1 CP |
| Food and Drink, Average, 1 Day  | 5 CP |
| Food and Drink, Superior, 1 Day | 2 SP |
| Trail Rations, 1 Day            | 5 CP |