# Skill Descriptions
This is the full list of skills in alphabetical order.

## Athletics
This broad skill covers a range of athletic activities useful to adventuring characters, including acrobatics, climbing, jumping and swimming.

## Artisanry (Type)
This skill covers several separate skills (such as armourer, baker, basket weaver, blacksmith, bowyer, brewer, butcher etc).
A character specializes in each of these areas separately.

## Close Combat
This skill deals with the art of hitting things and defending the character in melee with and without weapons.

## Crafting
This skill covers not only the successful application of dweomers, which the Crafter knows, but also the ability to manipulate the effects, range and duration of those dweomers.
This skill also allows the use of magic items with stored dweomers (commonly called Matrices) and scrolls with dweomers written on them.
It can also be used to represent the character's knowledge of Crafting and its works.

## Culture
Each Culture skill is used to provide information about the common world view of that group of people (or creatures).
This includes history, politics, weather cycles, geography, superstitions, and populat mythology.

Culture (Own) is the world view of the people that the character is born into; all other foreign or alien cultures are Culture (other).

Tests for Culture (Own) have a starting difficulty of Easy.

## Deception
Deception covers the arts of disguise (used to change a character's appearance and adopt a different outward persona), sleight (used to hide or take objects, without drawing undue attention), and stealth (used whenever a character attempts to personally evade detection by another character).

Deception tests are usually opposed by a reactive Intuition test using the Perception skill. 

## Dodge
This skill is used to avoid incoming objects that are swung or thrown at the character.

The Dodge skill is normally used when a character attempts to dodge an incoming blow in combat or a physical hazard that can be avoided, such as falling masonry.

## Healing
The use of Healing requires suitable medical equipment such as bandages or salves or appropriate improvised alternatives.
See the following table for the range of Healing actions available to the characters using this skill.

| Injury or Ailment | Treatment |
|:-----------------:|:----------|
| Unconsciousness   | Revive a character unconsciousness; drugged patients may inflict a penalty on the Healing test.
| Minor Injury      | Heal $$1\text{D}6$$ Hit Points
| Major Wound       | Stabilize patient so they will not die of blood loss; do not heal any hit points. A second test can be later performed to perform surgery and heal the character by 1 hit point; this will also allow normal healing to resume.
| Disease           | Add a bonus of $$\frac{1}{10}$$ healer's skill to patient's next opposed test to resist the disease.
| Poison            | Add a bonus of $$\frac{1}{10}$$ healer's skill to patient's next opposed test to resist the poison.

## Influence
This is the art of verbally persuading another character to do what you want.
Characters can use both logical and or emotional arguments.
Influence can never be used to get a character to act against their instinct for self-preservation.

Influence tests are normally opposed by a reactive Charisma test, often using the Perception, Persistence or Influence skills.
They are further modified by how much a character is trying to change an opponent's mind.

Influence tests are either applied to individuals, where each character rolls individually against the Influencer, or against crowds, were one roll is made to resist based upon an average Persistence for the entire crowd.

## Knackery
Knackery is a measure of how able a character is to exercise their particular, personal magic, their knacks.
Knacks are special abilities that manifest not as spells but as inherent capabilities, though they are the result of magic super-saturating the world.
Knacks are not, however, perfectly reliable, and using them saps something from the character.

For more information, see [CH5](../5-knackery/README.md).

## Language
The Language skill is actually several separate skills grouped under a single heading.
Language (English), Language (German), and Language (French), for example, are all individual skills.

Every character with a Language skill of 50% or more is fluent in that language, although they are likely to have an accent if it is not their native language.
A score in a Language skill of 80% or more will mean the character can also read and write in that language.
Even once a character has learned a language other than their Own they must still make tests to understand regional dialects of that language.
These tests have a starting difficulty of hard.

For Language (Own) a character is always considered fluent and may be able to read and write in that language, if appropriate to the culture.
Instead of representing a character's ability to speak their own language, Language (Own) represents the character's ability to understand regional dialects of their language accurately, though the tests do not start with the hard difficulty penalty.

## Lore
The Lore skill is actually an umbrella term for several different skills, each of which must be improved separately.

Each Lore skill defines an area of knowledge for the character and reactive tests are made whenever a player wants to see if their character knows something about the subject at hand.

Active tests for which the Lore skill might be applicable include research and interviews on the topic.

The range of possible Lores is only limited by a player's imagination.
A list of potential study areas of Lore is listed here:  alchemy, art, astronomy, gambling, geography, heraldry, law, logistics, military tactics, philosophy, poisons.

## Natural Lore
Natural lore is something all inhabitants of Tephra are at least somewhat versed in by nature of _living in the world_.
It can be broken into five specialist areas:

+ **Animal:** The ability to recognize an animal, know its feeding habits, breedinc cycle habits, etc, and to tame / train them.
+ **Plant"** The ability to identify plants in the wild, discover good places to grow crops, decide which plants are edible and what unusual properties they may possess.
+ **Mineral:** The ability to detect precious metals and stones, detect fault lines and other dangerous features in the rock.
+ **Survival:** The ability to live off of the land by finding food, water, and shelter, as well as tracking creatures.
+ **Weather:** The ability to predict changes in the weather.

## Perception
This skill is used to represent the five senses of the character when detecting objects or other characters.

## Performance (Type)
This skill covers acting, composing, poetry, dancing, singing, reading, playing instruments, etc.

## Persistence
This skill represents a character's mental willpower and is used to resist the effects of magic and often against another character's attempt to use the influence skill against them.

## Ranged Combat
This skill covers the use of missile weapons such as bows, crossbows, thrown spears and thrown daggers.
It is covered in more detail in [CH4](../4-combat/README.md).

## Relationships
During the course of their lives, characters develop relationships with non-player characters, groups and organizations.
These relationships can be positive or negative, and can aid or hinder a character's progression in the game.
In game mechanics terms there are four types of relationships to others.

+ **Ally**. This is a group or person a player character can ask for aid, can call in favours from, or with whom they otherwise have a positive relationship. 
+ **Dependent**. This is usually the character's family or a group that relies upon them.  
+ **Enemy**. This is a sworn enemy. Interactions with this person or group are usually negative.
  They may be a reccurring foe or ethnic enemy. 
+ **Organization**.This could be a Wizard's Fraternity, a Former Regiment or even a Cult.
  The character has an association with this organization; they typically deal with this group on a favourable basis.
  GMs should determine the maximum scale of an organization, which could be as great as a whole nation, or just a small wandering band of minstrels.

Relationships, like skills, have a score.
The player can use this in a number of ways depending on the relationship.
All relationships have a starting score of $$10$$ and relationship tests will almost always use Charisma for the base characteristic.

To gain a new relationship costs a number of Improvement Points, according to the table below.
Once a relationship is gained it is improved as a normal skill.
A character should have 2 relationships at the start of the game of their choice, as approved by the Game Master.

On a Critical relationship roll, the character gains a very positive response, and an increase of $$1\text{D}6\%$$ to their relationship.
On a fumble the player damages their relationship with the party and loses $$1\text{D}4\%$$ from their skill.
Further, after a fumble, the player cannot call on a relationship for rest of the adventure.
In the case of enemies this means that they have accidentally aided them in some way.  

For characters who are pursuing Supplication to an organization, their Supplication Skill supersedes their relationship.

| Relationship | IP Cost | Advantages (must make a successful relationship roll) | Disadvantages |
|:------------:|:-------:|:------------------------------------------------------|:--------------|
| Ally         | 2       | Loan an appropriate item or finances; assisst on social test; provide info and/or employment | Allies demand favours of the character, in the form of money, errands etc for each favour given; enemies of your ally become your enemies.
| Dependendent | 1       | Characters gain 1-3 additional Improvement Points where they support, aid or defend Dependents. | Dependents regularly get themselves and the character into trouble; Enemies will target dependents to harm the character; The loss of a Dependent can cause [Major Mental Damage](/8-Questing/Spot-Rules.md#major-mental-damage) if appropriate. 
| Enemy        | 2       | Gains +25% to tests in the attempt to thwart their enemy;  gain 1-3 additional IP for thwarting an enemy's plans; gain employment by enemy's opponents | Enemies will try to harm characters as often as possible; All interactions (parleys, influence, negotiation) with an enemy will be at one difficulty rating harder than normal.
| Organization | 2       | Can access the facilities offered by the organization; can gain information and possible employment | organizations typically demand a charge for their services, either in cash or a favour; Enemies of an organization may harry and harm the character for their relationship to the organization. 

## Resilience
This is a measure of how physically tough a character is.
The higher a character's Resilience, the more likely they are to handle adverse physical conditions, such as weathering a vicious sandstorm, surviving in a drought, or overcoming the effects of poison or disease.

## Sailing
This covers small water-borne craft propelled manually by oars or paddles, commonly known as boats, and larger craft powered by sail or rows of oars.
Travelling across calm water does not usually require a test but adverse conditions such as currents and weather can bestow penalties.

## Streetwise
This skill allows a character to find fences for stolen goods, black markets, and general information.
It also covers following people down crowded city streets without them being noticed.

## Supplication
Supplication is used to recall knowledge of about the organization they are supplicating to, including rituals, beliefs, history, members, etc.  

Becoming a supplicant to an organization requires a character to have a relationship skill of at least 50% with the organization.
It always counts as a supplicant skill.
A character can only be a supplicant of one organization at a time; this limit is magical, inherent to how supplication works.
The Supplication skill is not needed to actually perform Miracles but it is used to advance in status and power in the organization, and to grant access to higher magnitudes of Miracles. 

For more information, see [CH6](../6-supplication/README.md).

## Trade
This skill is primarily used when characters trade, barter or otherwise negotiate over the sale of goods.
In such transactions a successful Opposed Test using the Trade of the buyer versus the Trade of the seller is needed for the buyer to get the best deal.

If the buyer wins they gets a discount, -10% for a success, -25% for a critical.
If the seller wins to the price that they can sell the item for increases by +10% for a success and +25% for a critical.
If the opponent fumbles their roll, double the increase or decrease.

The Trade skill also enables the character to determine the value placed on something by others; estimating its market value.
Particularly common or obscure objects might give a bonus or penalty to the test.
Success will allow a character to guess the average monetary value of the object, normally guessing accurately to within 10% of its actual value.

## Wealth
This skill shows the resources and physical possessions a character has access to and is used to abstract wealth in game.
A character who has the equivalent Wealth level to a piece of equipment's purchase cost can automatically buy it.
If the piece of equipment's cost is one level above the character's Wealth, then the character will need to make a reactive Wealth test using either Charisma or Reason as the base characteristic (whichever is higher).

If they succeed the item is theirs.
Their next Wealth test is at $$-25\%$$ however.
This modifier lasts for a month.
If they fail they do not have enough spare cash at the time they try to buy the item, and must free up some of their savings or wait until they are paid again at the end of their current job.
If they Fumble they will find that they actually have a cash flow problem and cannot make any Wealth tests for at least a month, and they must actually spend time and effort sorting out their financial difficulties.
If they Critical they find they have more than enough disposable income, and can make their next Wealth test without a penalty.

> **[info] Table: What Wealth Means**
>
> | Wealth $$\%$$   | Description | Social Class               | Equipment | Housing |
> |:---------------:|:-----------:|:--------------------------:|:----------|:--------|
> | $$0$$ and lower | Destitute   | Beggars and serfs          | Rags, discarded tools, subsistence food | Streets or in a slum
> | $$01-34\%$$     | Poor        | Freemen laborers           | Basic clothes, knives, clubs; gruel and occasional meat; rents tools of trade. | In a crowded shared accommodation. If in city, owned by someone else
> | $$35-65\%$$     | Average     | Freemen with a trade       | Swords spear, shield, bow, crossbow, leather armour;  weekly meat; owns tools of trade. | Reasonable dwelling for self and immediate family, owned by them.
> | $$66-85\%$$     | Well-off    | Minor merchants, officials | Ring mail armour; good meat 2-3 times a week; owns a fine set of trade tools; has 1-2 servants or 2-4 serfs. | Owns a private residence for own family with room to spare.
> | $$86-90\%$$     | Wealthy     | Merchants, minor nobility  | Chain or plate armour; Choice of the best food; 2-12 servants or 3-18 serfs. | A villa with spacious rooms for extended family and servants.
> | $$100+\%$$      | Rich        | Kings, powerful nobles     | Custom made platemail; Regularly holds feasts; 3-18 hangers-on, body guard of 2-12 warriors, 50+ servants/serfs | A palace/castle with space for their family, hangers on plus staff.
> | $$200+\%$$      | Super Rich  | Emperors                   | Anything that can be bought | A grand palace complex which is the size of a small city, often within the walls of a larger outer city.