# Basic Tests
Before any test, the player first describes _what_ the character is trying to accomplish and _how_; this establishes the **intent and approach** and _only after_ the intent and approach are established will the GM decide whether or not a test is necessary.
GMs should **not** call for tests unless the _approach_ specified by the player could either fail or succeed to fulfill the _intent_ and even then only if failure will be of some importance.

If the GM decides that a test is called for, they will then specify the appropriate characteristic and whether the test is active or reactive.
The player should add their highest _relevant_ skill score to their goal.
In cases where it is ambiguous if a skill should apply, the GM will decide.

If the dice roll is equal to or less than the test score plus the relevant skill score the attempt is successful.
If the roll is greater than the total then it has failed.
The GM then describes the results of the test.

## Critical Successes
If the dice roll on a test is equal to or less than 10% of the goal then a critical success is achieved.
A critical success has an outcome that far exceeds the expectation of the player when the original test was made.
It's the best possible result based upon the player's original statement of intent.

The actual result of a critical success during a test is largely up to the GM.
It normally achieves one of the following results:
+ The task is completed sooner.
+ The task is completed to a higher degree of expertise than normal.
+ The task is completed with élan and style, generally impressing witnesses.
+ The character gains additional information or insight into the task thanks to their brilliance.

## Fumbles
Whenever a test results in a roll of 00, i.e. the two $$\text{D}10$$s both come up 0, then the player has rolled 100 rather than zero.
The character has _fumbled_ the roll.
A fumble is the worst imaginable outcome of the test based upon the player's original description of what their character was planning to do when the test was called for.

The actual result of a fumble is largely up to the GM to decide.
It normally results in one of the following mishaps:
+ The task takes twice as long to finish and is still a failure.
+ The task produces a useless result that actually makes further actions more difficult.
+ The task fails spectacularly, opening the character up to derision and scorn from witnesses.
+ The character becomes impeded or even harmed by their failure.

## Difficulty
Tests do not normally have an additional difficulty modifier.
However, when appropriate, a test can be either easy or hard.

If a test is _easy_, double the character's total for that test.
If a test is _hard_, halve the character's total for that test.

## Very High Skills
Characters with skills over 100% are considered Masters in their fields and under normal circumstances do not fail and quite often perform tasks that are considered impossible by normal people.

Unless the character is attempting a hard test they will almost always succeed.
However, the GM may still call for a test to be rolled if failure has actual consequences because even masters run the risk of fumbling.

## Assistance / Interference
A character can choose to make a test easier (for an ally) by assisting or harder (for an enemy) by interfering.
A character who wants to assist or interfere with a test needs to have a high enough score in the relevant skill.
No matter how high the character's skill, however, a character can't make a test easier than easy or harder than hard.
Characters' efforts to assist/interfere can cancel each other out.

### Assistance
| Initial Difficulty | Skill Score of Secondary Character | Effective Difficulty |
|:------------------:|:----------------------------------:|:--------------------:|
| Easy               | Any                                | Easy                 |
| Normal             | $$25+\%$$                          | Easy                 |
| Hard               | $$25+\%$$                          | Normal               |
| Hard               | $$75+\%$$                          | Easy                 |

### Interference
| Initial Difficulty | Skill Score of Secondary Character | Effective Difficulty |
|:------------------:|:----------------------------------:|:--------------------:|
| Easy               | $$25+\%$$                          | Normal               |
| Easy               | $$75+\%$$                          | Hard                 |
| Normal             | $$25+\%$$                          | Hard                 |
| Hard               | Any                                | Hard                 |