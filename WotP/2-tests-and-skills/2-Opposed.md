# Opposed Tests
Opposed tests are made by both characters who are in direct competition with each other.
Both characters make the tests as normal.

## One Character Succeeds
If one character succeeds their test and the other fails then the successful character has won the opposed test.

## Both Characters Succeed
If both characters succeed then whoever rolled the highest in their test wins the opposed test.
However, if one character rolls a critical while the other rolls an ordinary success then the character that rolled the critical wins.

## Both Characters Fail
Whoever rolled the lowest in their test wins the opposed test.
In the case of ties for both the Player wins.
If the action is player-against-player, the defender wins.