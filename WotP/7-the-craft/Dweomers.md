# Dweomers
The traits used by dweomers are detailed below.
+ **Concentration:** The dweomer's effects will remain in place as long as the character concentrates on it.
  Concentrating on a dweomer is functionally identical to applying the dweomer, requiring the Crafter to continue to gesture with both arms, chant and ignore distractions.
  This trait overrides the normal dweomer default Duration.
+ **Instant:** The dweomer's effects take place instantly.
  The dweomer itself then disappears.
  This trait overrides the normal dweomer default Duration.
+ **Permanent:** The dweomer's effects remain in place until they are dispelled or dismissed.
  This trait overrides the normal dweomer default Duration.
+ **Resist (Dodge/Persistence/Resilience):** The dweomer's effects do not take effect automatically.
  The target may make a Dodge, Persistence or Resilience test (as specified by the dweomer) in order to avoid the effect of the dweomer entirely.
  Note that Resist (Dodge) dweomers require the target to be able to use Reactions in order to Dodge.
  In the case of Area dweomers, the Resist (Dodge) trait requires the target to dive in order to mitigate the dweomer's effect.
+ **Touch:** Touch dweomers require the character to actually touch his target for the dweomer to take effect.
  The dweomer Crafter must remain in physical contact with the target for the entire applying.
  This trait overrides the normal Sorcery dweomer default Range.

## Dweomer List

| Dweomer                                               | Traits                                     | Description |
|:-----------------------------------------------------:|:-------------------------------------------|:------------|
| [Animate (Substance)](#animate-substance)             | Concentration                              | Animates inanimate substance, size dependent on Magnitude
| [Cast Back](#cast-back)                               |                                            | Shields the Crafter from hostile magic and has a chance of sending it back to the attacker.
| [Create Dweomer Matrix](#create-dweomer-matrix)       | Permanent                                  | Creates items that store dweomers
| [Create Scroll](#create-scroll)                       | Permanent                                  | Creates scrolls that store dweomers
| [Damage Boosting](#damage-boosting)                   | Touch                                      | +1 Magnitude to weapon damage
| [Damage Resistance](#damage-resistance)               | Touch                                      | Reduce incoming damage by magnitude
| [Diminish (Characteristic)](#diminish-characteristic) | Touch, Resist (Persistence/Resilience)     | Reduces a specified Characteristic by Magnitude
| [Dominate (Species)](#dominate-species)               | Resist (Persistence)                       | Gain control over a creature belonging to a specific species
| [Energy Projection (Type)](#energy-projection-type)   | Instant, Resist (Dodge)                    | Streams or balls of energy are projected at the target doing Magnitude in damage.
| [Enhance (Characteristic)](#enhance-characteristic)   | Touch                                      | Increases a specified Characteristic by Magnitude
| [Fly](#fly)                                           | Concentration, Resist (Persistence)        | Allow Crafter / Targets to fly
| [Form/Set (Substance)](#formset-substance)            | Instant                                    | Manipulate the form of a given substance
| [Glow](#glow)                                         |                                            | Causes a glowing point of light to appear on a surface
| [Haste](#haste)                                       |                                            | + Magnitude to Movement Rate, +10% per Magnitude to Initiative skill
| [Hinder](#hinder)                                     | Resist (Resilience)                        | - Magnitude to Movement Rate, -10% per Magnitude to Initiative skill
| [Holdfast](#holdfast)                                 | Touch                                      | Binds two surfaces together with STR equal to Magnitude
| [Magic Resistance](#magic-resistance)                 |                                            | If Magic Resistance's Magnitude is greater than the incoming effect's, then the incoming effect has no effect.
| [Mirage](#mirage)                                     |                                            | Creates an illusion based on all five senses
| [Mystic Vision](#mystic-vision)                       | Concentration                              | Allows recipient to literally see magic
| [Neutralise Magic](#neutralise-magic)                 | Instant                                    | Eliminates a combined Magnitude of magic effects equal to its own Magnitude, starting with the most powerful affecting the target.
| [Palsy](#palsy)                                       |                                            | Paralyse the target, if Magnitude is greater than the $$\frac{1}{4}$$ the target's HP
| [Protective Sphere](#protective-sphere)               | Resist (Resilience)                        | Create a sphere-shaped area of protection with a radius equal to Magnitude.
| [Regenerate](#regenerate)                             | Touch, Instant, Concentration Special      | Causes a severed or maimed limb to regrow or reattach.
| [(Sense) Projection](#sense-projection)               | Concentration                              | Project one sense anywhere within Range
| [Sense (Substance)](#sense-substance)                 | Concentration                              | Cause all sources of the substance within range to glow an appropriate colour visible only to the Crafter.
| [Skin of Life](#skin-of-life)                         | Touch                                      | Protects the recipient from suffocation by air deprivation, due to such factors as drowning or the Smother dweomer
| [Smother](#smother)                                   | Concentration, Resist (Resilience Special) | Neutralises the air surrounding the target, making each breath stale and worthless, depriving it of oxygen.
| [Tap (Characteristic)](#tap-characteristic)           | Touch, Concentration, Resist (Persistence) | Allows the Crafter to convert a Target's Characteristic points into Power Points.
| [Treat Wounds](#treat-wounds)                         | Touch, Instant                             | Repair one Hit Point per Combat Round per Magnitude for the Duration.
| [Venom](#venom)                                       | Touch, Resist (Resilience Special)         | Infuses Target with a poison of Potency equal to Magnitude X 5

### Animate (Substance)
+ Concentration

This dweomer allows the Crafter to animate one pound of the substance indicated, doubling in size with each point of magnitude.
The Crafter can cause it to move about and interact clumsily (Movement of 1 yard per three points of Magnitude).

The Crafter's chance to have the animated object perform any physical skill successfully is equal to his own chance to perform that action halved (before any modifiers).
If the appropriate Form/Set dweomer is applied immediately after this dweomer, the Crafter can perform much finer manipulation of the object.
In this case, the animated object will use the Crafter's full skill scores for physical activities.

This dweomer can only be used on inanimate matter.

### Cast Back
This protective dweomer shields the Crafter from hostile magic and has a chance of sending it back to the attacker.

Cast Back only affects magic dweomers, knacks, and miracles that target the user specifically and have the Resist trait.
Such effects may affect the protected character normally, but if it is resisted, the effect is launched back at the person who sent it, as long as its Magnitude is not greater than the Cast Back's Magnitude.

### Create Dweomer Matrix
+ Permanent

This dweomer creates items that store dweomers.

All dweomer matrices have an attached cost of 10 Gold Ducats per dweomer in special materials.

The enchanter pays 1 Improvement Point per dweomer stored in the matrix.

The wielder can apply and manipulate the dweomer at the skill of the original enchanter, but must still succeed on an easy Crafting test if the application is ritual.
Applying a dweomer through a matrix during combat reduces the difficulty of the test from hard to normal.

Dweomer matrices are reusable.

Dweomer matrices are mundane items in their own right and if the item is broken, then the dweomer is dispelled.
However at the time of enchantment the enchanter can spend another Improvement Point to magically harden the item doubling its Hit Points and Armor Points.

### Create Scroll
+ Permanent

These are written items which store dweomers.
All scrolls have an attached cost of 1 Gold Ducat per complexity of dweomer in ingredients for special inks/parchments, etc.
The resulting scroll is a one use item, which upon a successful Crafting test, applies the dweomer(s) with any manipulations, at the magnitude that was applied on the scroll.

Alternatively, upon a successful Crafting the reader of the scroll can learn the dweomer by spending the appropriate number of Improvement Points.
Either way, upon a successful use of the scroll, the dweomer fades from the scroll.
If the Crafting roll merely fails the dweomer remains, but the reader can not attempt to use the scroll until their Crafting skill increases.
If the Crafting roll is fumbled the dweomer fades from the scroll, without any benefit to the reader.

### Damage Boosting
+ Touch

This dweomer can be applied to any weapon up to five ENC.
Each point of Magnitude adds one point to the weapon's damage (the basic dweomer will increase a hatchet from $$1\text{D}6$$ damage to $$1\text{D}6+1$$ damage, for instance).

### Damage Resistance
+ Touch

This dweomer protects the body of the recipient.
Any incoming attack dealing damage equal to or less than the Magnitude of the dweomer is ignored.
Any incoming attack dealing more damage than the Magnitude of Damage Resistance is unaffected and will deal its full damage as normal.
Note that the protected character may still suffer from Knockback if applicable.

Damage Resistance may be combined with the Shield miracle in which case case, incoming damage is compared to the Damage Resistance dweomer first and only encounters the Shield miracle if the Damage Resistance is bypassed.

### Diminish (Characteristic)
+ Resist (Persistence/Resilience), Touch

There are actually seven Diminish dweomers, one for each Characteristic.
The dweomer will temporarily apply a penalty to the specified Characteristic equal to the Magnitude of the dweomer.
The penalty applied by this dweomer may not reduce a Characteristic below one and a creature must have the Characteristic in question to be affected by this dweomer.

Diminish (BOD / DEX) are resisted with Resilience. Diminish (INT, REA and CHA) are resisted with Persistence.

Applying a penalty to CHA does not reduce the character's Power Points.

### Dominate (Species)
+ Resist (Persistence)

This dweomer allows the Crafter to gain control over a creature belonging to a specific species.
If the target fails to resist the dweomer, it must obey the commands of the Crafter for the duration of the dweomer.

The controlled creature shares a telepathic link with the Crafter by which it can receive its orders.
If the Crafter and the creature dominated do not share a common language, the Crafter can order it about by forming a mental image of the actions he wishes the dominated creature to perform.

### Energy Projection (Type)
+ Ranged, instant, Resist (Dodge)

Energy is either projected as a beam or a ball towards the target, which can avoid the attack by Dodging.

If the dweomer takes effect the target takes damage equal to double the Magnitude of the dweomer.
Physical Armour does not protect against the damage, but magical protection does.
Types of energy that can be projected by this dweomer are Cold (Dark), Lightning, Heat (Fire), Shards of Rock (Earth), Windblast (Air).


### Enhance (Characteristic)
+ Touch

There are actually seven Enhance dweomers, one for each Characteristic.
Essentially the reverse of the Diminish dweomer, Enhance allows the Crafter to temporarily apply a bonus to the specified Characteristic equal to the Magnitude of the dweomer.
A creature must have the Characteristic in question to be affected by this dweomer.

Applying a bonus to CHA does not increase the character's Power Points.

### Fly
+ Concentration, Resist (Persistence)

Using this dweomer allows the Crafter (or whomever or whatever he targets with the dweomer) to fly.  The Crafter may levitate a number of objects or characters (the Crafter counting as one of these characters if he so wishes).

Objects and Characters must have an ENC lower than the Crafter's CHA characteristic.

Character or objects moved by this dweomer have a base Movement Rate of 6 yards / combat round.
All objects and character moved by this dweomer move at the Crafter's behest, not their own.

Each point of the dweomer's Magnitude may either be used to increase the target's Movement by +2y or to target an additional object or character – but not both.
A Crafter applying this dweomer at Magnitude 4 may fly himself with a Movement of 14y, fly himself and a friend with a Movement of 10y each, or fly himself and three friends with a Movement of 6y each.

### Form/Set (Substance)
+ Instant

There are an unlimited number of Form/Set dweomers in existence, one for every substance imaginable, from steel to smoke to water.

Each point of Magnitude allows the Crafter to shape one ENC of solid substance or one cubic yard of an ethereal substance (like darkness).
The Crafter must be familiar with the shape he is forming.

When the Crafter has finished the forming process, the substance retains its shape.
Rigid substances like steel will hold the form they had at the end of the dweomer, while more mutable substances like water will immediately lose their shape.

This dweomer can be used to mend damage done to an object.
The Crafter must form the entire object and must succeed at an appropriate Craft test.
If successful he will restore the item to its original condition.

This dweomer can only be used on inanimate substances.

### Glow

This dweomer causes a glowing point of light to appear on a solid substance.
At its base, the dweomer creates an area of light one yard in radius, giving off the same illumination as a candle.
Each additional point of Magnitude increases the radius of effect by one yard.
At Magnitude 3, the brightness of the dweomer increases to that of a flaming brand at its centre.
At Magnitude 5, it increases to that of a campfire and at Magnitude 10 to that of a bonfire.

This dweomer can be cast on an opponent's eyes.
If cast on a living being the dweomer also gains the Resist (Dodge) trait.
If the target fails to resist it, he will suffer a penalty to all attack, parry and Dodge tests, as well as any skills relying upon vision, equal to five times the dweomer's Magnitude, until the dweomer ends or is dispelled.

### Haste

Each point of Magnitude of Haste adds 1 yard to the Movement rate of the recipient and +10% to the recipient's Initiative skill.

### Hinder
+ Resist (Resilience)

Each point of Magnitude of Hinder subtracts 1 yard from the Movement rate of the target and inflicts -10% penalty to the victim's Initiative skill.

### Holdfast
+ Touch

This dweomer causes two adjacent square foot surfaces (roughly the size of a man's palm) to commingle into one.
The basic bond has a STR of 1.
Each additional point of Magnitude will either increase the STR of the bond by +1 or double the area affected.

This dweomer can affect organic and inorganic substances.
If the Crafter is attempting to bond a living being with this dweomer, the dweomer gains the Resist (Resilience) trait.

### Mirage

This is the Crafters' version of Illusion.

This dweomer creates an illusion based on all five senses.
The illusion will seem real and solid unless the person looking at it succeeds in a Perception test, which is subject to a modifier based on the Magnitude of the dweomer.
If the viewer succeeds in a Perception test and the Illusion could usually cause damage if believed in, it can no longer cause damage to that character.
As soon as a viewer disbelieves the illusion it becomes insubstantial and ghost like to him.

The Size of the illusion is also governed by the magnitude.
A Magnitude 1 Illusion can be used to create small household items, say a fake table and chair, but would not be able to create an illusion of a fire breathing Dragon.

| Magnitude | Perception Test Modifier | Type of Illusion Possible |
|:---------:|:------------------------:|:--------------------------|
| 1         | +50%                     | Not capable of motion or causing damage. Slightly fuzzy and unreal round the edges. No larger than 5 cubic yards.
| 2         | +25%                     | Some minor discrepancies. Capable of motion, but not of damage. No larger than 10 cubic yards.
| 3         | -                        | Capable of motion and causing damage. No larger than 15 cubic yards.
| 4         | -25%                     | Capable of motion and causing damage. No larger than 20 cubic yards.
| 5         | -50%                     | Indistinguishable from the real thing, capable of motion and damage. No larger than 25 cubic yards.
| +1        | -50%                     | +5 cubic yards to size per Magnitude

### Mystic Vision
+ Concentration

This dweomer allows the recipient to literally see magic.
By augmenting the recipient's natural vision, the dweomer allows him to see a creature's Power Points, as well as enchanted items with their own Power Points, Knacks, Miracles, or Dweomers.
The recipient must be able to actually see the creature or object for this dweomer to work.

On a normal success the recipient of the dweomer will only know roughly how many Power Points an object or creature has (1–10, 11–20, 21–30 and so forth).
On a critical they will know exactly.
On a fumble the Games Master should give the player a misleading total.

By looking at a magic effect, a recipient of Mystic Vision will automatically be aware of its magical origin (knackery, supplication, or the Craft).
By increasing the Magnitude of Mystic Vision, the Crafter can learn more about what he is seeing.
Compare the Magnitude of Mystic Vision to the Magnitude of any dweomer that the target is either casting or under the influence of.
As long as Mystic Vision's Magnitude exceeds the other dweomer's, the Crafter will be able to precisely determine the effects of the perceived dweomer, and a mental image of who applied the dweomer (if it is not obvious).

By looking at an enchanted item, a recipient of Mystic Vision will automatically be aware of its gross magical effects (such as the types of enchantment currently on the item).
Each point of Magnitude of Mystic Vision will also determine either the invested CHA (and therefore the relevant strength) of a particular enchantment or a particular condition laid upon an enchantment, at the Games Master's choice.

### Neutralise Magic
+ Instant

This dweomer allows a Crafter to neutralise other magic effects.
Neutralise Magic will eliminate a combined Magnitude of effects equal to its own Magnitude, starting with the most powerful affecting the target.
If it fails to eliminate the most powerful effect then it will instead target the second-most powerful effect.
As soon as Neutralise Magic can no longer dismiss a target's effects, because all the remaining effect's Magnitudes are too high), its effects immediately end.

Neutralise Magic can be fired as a Reaction, but only when another effect is used within Neutralise Magic's Range that the character wishes to counter.
A successful Neutralise Magic disrupts the other effect and nullifies it.
As long as Neutralise Magic's Magnitude equals or exceeds the target effect's Magnitude, the target effect is countered.

### Palsy
+ Resist (Resilience)

If the Crafter is able to overcome his target with this dweomer, he can turn the victim's own nervous system against him.
The dweomer will paralyse the target, provided the dweomer's Magnitude is greater than one quarter of target's current Hit Points.

### Protective Sphere

When completed, the Protective Sphere will create a sphere-shaped area of protection with a radius in yards equal to the dweomer's Magnitude.
If this dweomer is applied on the ground (or other immovable place) it cannot be moved.
If applied on a vehicle (such as the bed of a wagon) or a person, it will move with the target.
After the sphere has been completed any one or all of the following dweomers can be added to provided the defensive capacities of the Sphere during the duration of the Sphere.
The Sphere on its own provides no protection, that is down to the Resistance dweomers.

+ Damage Resistance
+ Dweomer Resistance

The Protective Sphere's perimeter contains the benefits of its combined Resistance dweomer(s).
The Protective Sphere only inhibits effects or attacks entering the circle from the outside – attacks or effects originating within the circle are unaffected.

### Regenerate
+ Concentration Special, Instant, Touch

This dweomer causes a severed or maimed limb to regrow or reattach.

The Magnitude of the dweomer must equal or exceed the maximum Hit Points lost as a result of the Major Wound taken.
This dweomer will cause a limb severed by a Major Wound, if present, for the limb to reattach itself to its stump.

Regenerate takes a number of miniutes equal to the target's HP damage that caused the Major Wound to reattach a limb, during which time the Crafter must maintain concentration on the dweomer.
The Hit Points lost due to the Major Wound are recovered at the end of this period.

As with all magic used to heal, the Crafter must succeed on a Healing skill test to correctly identify what needs to be done to fix each wound.
This test, however, is hard.

### (Sense) Projection
+ Concentration

Each (Sense) Projection dweomer is a separate dweomer.
These dweomers encompass the five base senses but there are also variants for any unusual sensory mechanism appropriate to the game world (such as sonar).

This dweomer allows the Crafter to project one of his senses anywhere within the dweomer's Range.
The dweomer forms an invisible and intangible sensor, one foot across, which receives the specified type of sensory input and transmits it to the Crafter.
The sensor can move a number of yards per Combat Round equal to the dweomer's Magnitude at the Crafter's direction and allows him to use his Perception skill through the sensor.

Dweomers can be cast through the sensor of some Projection dweomers.
For instance, ranged dweomers require Sight Projection, while touch dweomers require Touch Projection (and likely Sight Projection too, simply so the Sorcerer can find his target efficiently).

Characters using Mystic Vision can see the sensor and attack it if they wish, though it is only vulnerable to magic.
Magical weapons and dweomers employed against the sensor will not destroy it but will instead transfer their damage directly to the Crafter.

### Sense (Substance)
+ Concentration

Eminently useful for finding valuables from afar, this dweomer has a variation for every substance imaginable.
Sense (Substance) will cause all sources of the substance within range of the dweomer to glow an appropriate colour visible only to the Crafter – diamonds will gleam like ice, amber will shine like a camp fire and so on.
Each point of this dweomer's Magnitude allows it to penetrate one yard of rock, wood or dirt.
If the source is concealed behind such a material, the surface nearest the Crafter will glow for a moment.
The dweomer cannot penetrate refined metal, though it can penetrate ore.

### Skin of Life
+ Touch

This dweomer protects the recipient from suffocation by air deprivation, due to such factors as drowning or the Smother dweomer.
Each point of Magnitude will cover one cubic yard.

### Smother
+ Concentration, Resist (Resilience Special)

If successful, this dweomer neutralises the air surrounding the target, making each breath stale and worthless, depriving it of oxygen.
The Crafter must concentrate each round in order to keep the dweomer operating.
For the duration of the dweomer, the target will be unable to breathe, essentially drowning on dry land.

When the dweomer begins, the target's Resilience test determines whether it is able to gasp in one last breath before Smother cuts off the surrounding oxygen supply.
If the target succeeds, it may hold its breath as normal.
If it fails, it will start drowning in the next Combat Round.

This dweomer can also be used to extinguish fires, as the flames will be starved of oxygen.
At Magnitude 1, it can extinguish a Flame, Magnitude 2 a Large Flame, Magnitude 4 a Small Fire, Magnitude 7 a Large Fire and Magnitude 10 it will put out an Inferno.
Smother has no effect on magical fire or on fire-based creatures.

### Magic Resistance

This dweomer matches its Magnitude against the Magnitude of any incoming dweomer. If Magic Resistance's Magnitude is greater than the incoming effect's, then the incoming effect is nullified.
If the incoming effect's Magnitude is equal to or greater than the Magnitude of Magic Resistance, then the effect applies as normal.

Unlike many protective abilities, Magic Resistance remains in place for the entirety of its Duration – effectsd that successfully breach the dweomer do not dispel it.
However, it does not discriminate between incoming effects – a comrade attempting to magically heal the recipient of Magic Resistance must overcome it in order to successfully use a healing ability.

### Tap (Characteristic)
+ Concentration, Resist (Persistence), Touch

There are actually several Tap dweomers, one for each Characteristic.
These devastating dweomers allow the Crafter to permanently strip a target of Characteristic points, transforming them into Power Points for his own use.

The Crafter must make contact with the target, either physically or through Touch Projection, in order to Tap it.

Tap will only work if its Magnitude is equal to, or greater than the target's specified Characteristic.
Thus a Magnitude 6 Tap Charisma dweomer would only work on targets with a CHA of 6 or lower.

The number of points Tapped by the dweomer are equal to $$1\text{D}6$$ per Combat Round the dweomer is applied to the Victim.

Characteristic points lost to Tap are lost permanently, though the victim can raise them again through normal means of increasing a Characteristic.
Characteristics may be Tapped to 0, which usually involves the death of the victim.

For each Characteristic point the Crafter Taps, he will gain one Power Point.
The Crafter is limited in the number of Power Points he can gain through Tap; the dweomer can only increase his Power Points to double his normal limit.
A Crafter may simply Tap a target and dissipate any gained Power Points.

If the Crafter gains more Power Points through Tap than his normal maximum, they will disappear at the rate of one Power Point per minute once the dweomer finishes.

### Treat Wounds
+ Instant, Touch

This dweomer must be cast upon a wounded character.
It dramatically accelerates the natural healing rate of the target.
For every point of Magnitude of this dweomer, the caster can repair one hit point per Combat Round, for the Duration of the dweomer.
Treat Wounds cannot reattach or regrow a severed limb and will not work on any  Major Wound.

As with all magic used to heal, the Crafter must succeed on a Healing skill test to correctly identify what needs to be done to fix each wound.

### Venom
+ Resist (Resilience Special), Touch

This dweomer infuses the target's body with a magical poison.
The Potency of the poison is equal to the dweomer's $$Magnitude \times 5$$, takes effect instantly, and does damage equal to the magnitude per combat round for the dweomer's Duration.
The target may resist the poison with a Resilience test, as normal.