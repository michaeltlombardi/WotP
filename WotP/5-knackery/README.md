# Knackery

All characters in WotP have access to knackery, the special abilities that become a part of them due to the magic of the world and the characters' own beliefs, experiences, and actions.

## Power Points
While knacks are inherent to all characters, using them can sometimes be draining and not all characters are able to exercise their knacks as often as more powerful characters.

All characters start play with Power Points equal to their CHA Characteristic score.
A character's CHA score also axts as a maximum limit to the amount of Power Points a character can store at any one time.

Characters can draw magical power from their surroundings with a Persistence test, regaining $$1\text{D}6$$ Power Points.
At the GM's discretion, some locations may not have as much ambient magic to draw on, making the test Hard or even impossible.
Alternatively, some places may be overflowing with magic power and the test to draw on it Easy.
For each successive draw the persistance test incurs a $$-25\%$$ cumulative penalty.
If the character fails to draw power they become fatigued instead from the strain.
Fatigued characters may not attempt to draw power from the environment.

Characters may also have magic items that act as Power Point stores (see Create Power Point Store).

A character who is reduced to zero Power Points falls unconscious until they have regained one Power Point.

### Regaining Power Points
Power Points regenerate once the character fully rests, either by sitting down and taking it very easy or by having a good night's sleep.

For every two hour period that a character rests they regain Power Points equal to $$\frac{1}{4}$$ their CHA total.

## Acquiring Knacks
Knackery is treated as a skill with a starting score of $$CHA \times 3$$.

All characters start play with their knackery skill at base and 6 points of Magnitude of knacks.

Characters don't learn knacks, they acquire them in the course of holding strong beliefs and performing actions and becoming more powerful versions of their own ideals.
It costs one Improvement Point per Magnitude point to acquire a knack (Improvement points are covered in [CH8](../8-Questing/README.md)).
If a character has a knack at a lower Magnitude they only have to pay the difference in Improvement Points to gain the knack at a higher Magnitude.

Knacks can stem from a variety of sources of belief about the world:

+ From local folklore and tradition
+ From religious or philosophical beliefs
+ From studying magic
+ From the work and thought patterns of a profession or organization
+ From deeply held personal beliefs

In each case the player should have a relatively good explanation for how the knack ties into a belief or quality the character possesses.

Sometimes it is useful to have characters only gain new knacks at the completion of a quest or only with points gained from fulfilling a motive, but this is entirely optional.

## Exercising Knacks
All knacks take one combat round to exercise and do not require a roll to determine if they go off (though they may require a roll anyway, as with a touch attack or requiring the target to make a resistance check).

Distractions or attacks on the character as they exercise their knack will automatically ruin the attempt (costing the exercising character one Power Point) unless the exercising character successfully passes a Persistence test.
Examples of distraction include blinding, disarming, or wounding the character.

In a single Combat Round a character can dismiss any Permanent knack(s) they have exercised as a free action.
Ceasing to exercise a Concentration knack is immediate and not an action.