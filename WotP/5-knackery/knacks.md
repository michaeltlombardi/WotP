# Knacks
Unless otherwise stated all Knacks have the following traits.
+ They have Variable Magnitude.
  This means that the Magnitude of the knack starts from the stated Magnitude and then can be exercised at a higher Magnitude, if the character knows it, giving an increase in the effect of the knack.
  The maximum Magnitude that a character can learn is equal to $$\frac{1}{10}$$ their Knackery skill score.
+ Base Magnitude is one.
+ Range is equal to the character's $$CHA \times 3$$ in yards.
+ All knacks, unless noted, have a Duration of ten minutes.
+ Other traits used by knacks are detailed below.
  + Area (X): The knack affects all targets within a radius specified in yards.
  + Concentration: The knack's effects will remain in place so long as the character continues to concentrate on it.
    Concentrating on a knack is functionally identical to exercising the knack, requiring the character to continue to focus and ignore distractions.
  + Instant: The knack's effects take place instantly.
    The knack itself then disappears.
  + Magnitude (X): The strength and power of the knack.
    Also the minimum number of Power Points required to exercise it.
  + Non-Variable: The knack may only be exercised at the stated Magnitude.
  + Permanent: The knack's effects remain in place until they are dispelled or dismissed.
  + Passive: The knack is always in effect and cannot be dispelled or dismissed.
  + Prerequisites: This knack has one or more prerequisite conditions which must be met before the knack can be acquired.
  + Resist (Dodge/Persistence/Resilience): The knack's intended effects do not succeed automatically.
    The target may make a Dodge, Persistence or Resilience test (as specified by the knack) in order to avoid the effect of the knack entirely.
    Note that Resist (Dodge) knacks require the target to be able to use Reactions in order to Dodge.
    In the case of Area knacks, the Resist (Dodge) trait requires the target to dive in order to mitigate the knack's effect.
  + Touch: Touch knacks require the character to actually touch his target for the knack to take effect, using a close combat skill test to make contact.
    The character must remain in physical contact with the target for the entire casting.

## Knacks List

| Knack                                                 | Traits                                                         | Effect |
|:-----------------------------------------------------:|:--------------------------------------------------------------:|:-------|
| [Animal Whisperer](#animal-whisperer)                 | Magnitude 2, Non-Variable, Touch                               | Character whispers into the ear of a distressed animal, calming it.
| [Avoidance](#avoidance)                               | Instant                                                        | Allows character to Dodge as many times as the spell's Magnitude.
| [Babel](#babel)                                       | Magnitude 2, Non-Variable, Resist (Persistence)                | Garbles the language of the affected creature.
| [Bearing Witness](#bearing-witness)                   | Instant                                                        | $$10\%$$ per Magnitude bonus to next Skill Test.
| [Beast Call](#beast-call)                             | Magnitude 2, Non-Variable, Instant, Resist (Resilience)        | Attract an animal within range.
| [Befuddle](#befuddle)                                 | Magnitude 2, Non-Variable, Resist (Persistence)                | Confuses the target so they cannot attack.
| [Block Sense (Sense)](#block-sense-sense)             | Magnitude 3, Non-Variable, Resist (Persistence)                | Blind/Deafen/Bland taste/Numb touch.
| [Care](#care)                                         | Magnitude 2, Non-Variable, Touch                               | Recipient gets benefit of character's Protection or Countermagic knacks.
| [Clear Path](#clear-path)                             | Touch                                                          | Clears teh way through dense undergrowth.
| [Coordination](#coordination)                         | Touch                                                          | $$+10\%$$ per Magnitude to Initiative, Dodge and Athletics
| [Counter-Attack](#counter-attack)                     | Magnitude 2, Non-Variable                                      | Allows a counter-attack in addition to any other Reactions.
| [Counter-Defence](#counter-defense)                   | Magnitude 2, Non-Variable                                      | Allows an extra reaction in addition to any other reactions.
| [Countermagic](#countermagic)                         | Instant                                                        |  Used as a combat reaction, automatically dispels any magic cast within range whose Magnitude is lower or equal to its own.
| [Cover Blind Side](#cover-blind-side)                 | Magnitude 1, Non-Variable                                      | Recipient can react to attacks from behind as if it was a normal attack from front.
| [Create Charms](#create-charms)                       | Permanent                                                      |  Create magic items imbued with some measure of a knack.
| [Create Power Point Store](#create-power-point-store) | Permanent                                                      | Create a Power Point Store which can later be used instead of the character's own Power Points. One point per Magnitude.
| [Create Potion](#create-potion)                       | Permanent                                                      |  Creates potions imbued with some measure of a knack.
| [Cushion Fall](#cushion-fall)                         |                                                                | Each point of Magnitude elimintates one dice of falling damage for the recipient.
| [Darkwall](#darkwall)                                 | Area 5, Magnitude 2, Non-Variable                              | Create a moveable solid wall of darkness.
| [Demoralize](#demoralize)                             | Magnitude 2, Non-Variable, Resist (Persistence)                | If exercised before combat begins, target loses will to fight. If exercised during combat, all tests for target are Hard and they may not utilize offensive knacks.
| [Detect X](#detect-x)                                 | Concentration, Non-Variable                                    | Where X is a substance or a group of living beings.
| [Dispel Magic](#dispel-magic)                         | Instant                                                        | Dispels spells of a Magnitude equal or lower to it.
| [Disruption](#disruption)                             | Instant, Resist (Resilience)                                   | Causes $$1\text{D}4$$ damage per Magnitude
| [Dragon's Breath](#dragons-breath)                    | Magnitude 2, Non-Variable, Instant, Resist (Dodge)             | Character breaths a stream of fire for $$1\text{D}10$$ damage.
| [Dull Weapon](#dull-weapon)                           |                                                                | Reduce a weapon's damage by one point per Magnitude.
| [Enhance Skill X](#enhance-skill-x)                   |                                                                |  Adds $$+10\%$$ to a particular skill.
| [Extinguish](#extinguish)                             | Instant                                                        | Puts out fires.
| [Extra Defence](#extra-defense)                       | Ranged                                                         | Each point gives an extra defensive reaction.
| [Fanaticism](#fanaticism)                             | Magnitude 2, Non-Variable                                      | $$+25\%$$ to close combat skill but may not parry or cast spells.
| [Firearrow](#firearrow)                               | Magnitude 2, Non-Variable                                      | Affected arrow does $$1\text{D}10$$ magical flame damage.
| [Fireblade](#fireblade)                               | Magnitude 4, Touch, Non-Variable                               | Affected weapon does $$1\text{D}10$$ magical flame damage.
| [Fist of Gold](#fist-of-gold)                         |                                                                | $$1\text{D}10$$ Gold Ducats per Magnitude for the duration of the spell.
| [Fist of the Wind](#fist-of-the-wind)                 | Instant                                                        | One extra unarmed / grapple attack per Magnitude.
| [Flying Kick](#flying-kick)                           | Magnitude 2, Non-Variable                                      | Normal move through air, followed by an attack.
| [Frostbite](#frostbite)                               | Magnitude 2, Non-Variable, Instant, Resist (Resilience)        | $$1\text{D}8$$ cold damage to target.
| [Glue](#glue)                                         | Touch, Area                                                    | Covers an area with extremely sticky glue.
| [Hand of Death](#hand-of-death)                       | Instant, Magnitude 4, Non-Variable, Resist (Resilience), Touch | Reduced Hit Points by half and inflicts a Major Wound.
| [Harden](#harden)                                     | Magnitude 1, Non-Variable, Instant                             | Makes the target item unbreakable.
| [Heal](#heal)                                         | Instant, Touch                                                 | +1 Hit Point per point of magnitude Magnitude, but must understand what they're healing.
| [Hinder Skill X](#hinder-skill-x)                     | Ranged, Resist (Persistence)                                   | $$-10\%$$ to target's given skill per point of Magnitude.
| [Ignite](#ignite)                                     | Magnitude 1, Non-Variable, Instant                             | Starts fires.
| [Invisibility](#invisibility)                         | Magnitude 5, Non-Variable, Concentration, Touch, Personal      | Make the recipient invisible.
| [Ironmind](#ironmind)                                 | Touch                                                          | $$+10\%$$ per Magnitude vs Spells and Knacks.
| [Knockback](#knockback)                               | Instant, Resist (Resilience)                                   | Knocks victim back Magnitude in yards.
| [Knockdown](#knockdown)                               | Instant, Magnitude 2, Non-Variable, Resist (Resilience)        | Knocks target prone.
| [Leap](#leap)                                         | Touch, Resist (Dodge)                                          | Jump 2 yards up in air per point of Magnitude.
| [Light](#light)                                       | Magnitude 1, Non-Variable, Area 10y                            | A magical light that illuminates its area.
| [Mindspeech](#mindspeech)                             |                                                                | Allows mental communication with one target per point of Magnitude.
| [Mobility](#mobility)                                 |                                                                | +2 yards to Movement Rate per Magnitude
| [Multi-Attack](#multi-attack)                         | Instant                                                        | One extra attack per magnitude
| [Personal Insight](#personal-insight)                 | Magnitude 2, Non-Variable                                      | Answers one question relevant to the character.
| [Pierce](#pierce)                                     | Touch                                                          | -1 AP per Magnitude
| [Protection](#protection)                             |                                                                | +1 AP per Magnitude
| [Read Emotion](#read-emotion)                         | Instant, Magnitude 1, Non-Variable, Resist (Persistence)       | Allow the character to know the true emotional state of the Target
| [Resist (Element)](#resist-element)                   |                                                                | $$+10\%$$ per Magnitude to resist a given element.
| [Restore Energy](#restore-energy)                     | Touch, Instant                                                 | Each Magnitude of this knack restores one Fatigue level.
| [Sap Energy](#sap-energy)                             | Touch, Instant, Resist (Resilience)                            | Inflict one Fatigue level per Magnitude on the target.
| [Scare](#scare)                                       | Magnitude 2, Non-Variable, Resist (Persistence)                | Target must move away from the character
| [Second Sight](#second-sight)                         | Magnitude 3, Non-Variable                                      | Allows the character to judge how many Power POints a target has.
| [Skybolt](#skybolt)                                   | Instant, Magnitude 3, Resist (Dodge)                           | A bolt of electrical energy that does $$2\text{D}6$$ damage and ignores non-magical Armor.
| [Slow](#slow)                                         | Resist (Resilience)                                            | -2 yard to Movement Rate per Magnitude.
| [Speedart](#speedart)                                 | Touch, Magnitude 2, Non-Variable, Trigger                      | +3 damage, $$+25\%$$ to Ranged Combat skill
| [Strength](#strength)                                 | Touch                                                          | $$+10\%$$ to any strength based Athletics test and Damage per point of Magnitude
| [Tallk to Animal](#talk-to-animal)                    | Magnitude 3, Non-Variable                                      | Character can talk to any animal within ten yards
| [Thunder's Voice](#thunders-voice)                    |                                                                | $$+10\%$$ to Influence per Magnitude
| [Tongues](#tongues)                                   | Magnitude 2, Non-Variable                                      | Allows the recipient to talk a given language/
| [Unlock](#unlock)                                     | Touch, Instant                                                 | Unlocks locked items. Base chance = $$\text{Magnitude} * 20\%$$
| [Vigor](#vigor)                                       | Touch                                                          | +2 Hit POint per point of Magnitude for the duration of the spell.
| [Vomit](#vomit)                                       | Ranged, Resist (Resilience)                                    | Target vomits for Magnitude rounds, takes $$1\text{D}6$$ damage if resist roll fumbled.
| [Walk on (Element)](#walk-on-element)                 | Magnitude 3                                                    | Allows character to walk on given element without harm
| [Water Breath](#water-breath)                         | Touch                                                          | Allows character to breathe underwater.
| [Weapon Enhance](#weapon-enhance)                     | Touch                                                          | $$+10\%$$ to hit, +1 Damage with weapon cast on. Extra damage is magical.

## Knack Descriptions

### Animal Whisperer
+ **Traits:** Touch, Magnitude 2, Non-Variable
+ **Description:** The character whispers into the ear of a distressed animal, calming it.
  If the distressed animal is under the influence of a knack such as Fear or Scare, then its gets another Persistence test to shake off the effect of the knack.

### Avoidance
+ **Traits:** Instant
+ **Description:** This knack lies dormant until the recipient is attacked.
  Then, after the normal reaction of the recipient, it fires off allowing the recipient to Dodge a number of times equal to the knack's Magnitude.
  Once triggered, all the points of the spell are fired off at once.

### Babel
+ **Traits:** Magnitude 2, Non-Variable, Resist (Persistence)
+ **Description:** This knack garbles the language of the affected creature.
  The target can still think and, for the most part, act normally, but anything it says comes out as gibberish.
  Thus, a commanding officer would be unable to give orders to his men and a Crafter would be unable to cast spells.

### Bearing Witness
+ **Traits:** Instant
+ **Description:**This knack grants the character a $$+10\%$$ bonus per point of Magnitude to their next Skill Test they make to discover lies, secrets, or hidden objects.
It does not stack with any other knack or spell effect bonuses.

### Beast Call
+ **Traits:** Instant, Magnitude 2, Non-Variable, Resist (Resilience)
+ **Description:** This knack serves to attract an animal within range.
  When the knack is exercised it affects a targeted creature with a fixed INT of 7 or less.
  If it fails to resist, the creature will be naturally drawn to the place where the knack is exercised, whereupon the effect terminates.
  Any barrier, immediate threat, or counter control, also ends the effects of the knack, leaving the creature to react naturally.

### Befuddle
+ **Traits:** Magnitude 2, Non-Variable, Resist (Persistence)
+ **Description:** This knack prevents a target from casting spells, exercising knacks, and taking offensive actions.
  The target my run if it so chooses and may dodge and parry normally in combat, though it may not make any attacks unless it is attacked first.

### Block Sense (Sense)
+ **Traits:** Magnitude 3, Non-Variable, Resist (Persistence)
+ **Description:** This knack will Blind/Deafen/Bland teste/Numb touch on a failed resistance roll for the duration of the spell.

### Care
+ **Traits:** Touch, Magnitude 2, Non-Variable
+ **Description:** If the character has any active Protection or Countermagic knacks, the Cared for character _also_ benefits fromt he effects of these knacks.

### Clear Path
+ **Traits:** Touch
+ **Description:** This knack allows the character to move through even the most tangled, thorny brush as if they were on an open road.
  For each additional point of Magnitude, they may bring one person with them.

### Coordination
+ **Traits:** Touch
+ **Description:** For every point of Magnitude the target gains a $$+10\%$$ bonus to initiative, dodge, and DEX-based Athletics tests.

### Counter-Attack
+ **Traits:** Magnitude 2, Non-Variable
+ **Description:** This knack lies dormant until the recipient is attacked.
  Then, after the normal defensive reaction of the recipient, it fires off, allowing the recipient to follow up with a counter attack.
  The counter attack is an additional action, on top of the recipients normal attacking action.

### Counter-Defense
+ **Traits:** Magnitude 2, Non-Variable
+ **Description:** This knack lies dormant until the recipient is attacked.
  Then, after the normal defensive reaction of the recipient, it fires off, allowing the recipient an extra defense.

### Countermagic
+ **Traits:** Instant
+ **Description:** This knack is only used as a Reaction, and only when a spell is cast within Countermagic's Range that the character wishes to counter.
  It must be declared during the initiative phase as normal.
  Countermagic disrupts the other spell and nullifies it as long as Countermagic's Magnitude equals or exceeds the target spell's Magnitude.

### Cover Blind Side
+ **Traits:** Magnitude 1, Non-Variable
+ **Description:** For the duration of the spell the target can react to attacks from behind or flank attacks as if they were a normal attack from the front.
  It does not confer any additional reactions.

### Create Charms
+ **Traits:** Permanent
+ **Description:** A charm is a physical item that stores one or more knacks that the character possesses.
  A charm is a representation of the character's incredible skill and abilities when applied to artisanry.
  A charm could be a necklace that holds a Befuddle 4 knack, a sword invested with the will of its smith and so holds a Weapon Enhance 2 knack, or even a sheet of paper with a poem writton it that, when held agains the skin, provides a Protection 1 knack.

  To create a charm a character must possess both the knack they wish to imbue and Create CHarm at the same Magnitude.

  If the character spends one Improvement Point at the time of creation the knack within the Charm is reusable.
  Otherwise, once the knack is exercised, the Charm loses all potency.

  A knack stored in a Charm is used like any other Knack the possessor has acquired.
  It uses the wielder's Knackery skill and is powered by the wielder's Power Points.

  The time taken to create a single-use Charm is one hour per point of Magnitude of the knack being imbued.
  Reusable Charms take three hours per point of Magnitude to create.

  Charms are mundane items in their own right and if the item is broken the Charm is lost.

### Create Power Point Store
+ **Traits:** Permanent
+ **Description:** This knack allows the character to create an item which as Power Point storing capabilities.
  This allows the owner to have a pool of Power Points in addition to their own.

  Typically, crystals are used due to their physical toughness - in game terms, treat them as unbreakable.
  This also applies to charms, such as a sword with Weapon Enhancement 2 stored in it, to provide a pool of power points to exercise the knack from.

  Power Point stores take one hour per power point stored in them to create.
  For each Magnitude, one power point can be stored.

  Unless one improvement point is spent when they are created they are non-reusable.
  Once the Power Points are used the item loses its ability to store Power points.
  If the improvement point is spent the item then becomes reusable.
  Once all the Power Points are used, the item can be refilled instantly from the user's own Power Points.

  The character must fill the item with their own Power Points as part of the knack.
  The amount of Power Points put into the item at the time of casting becomes the maximum that can be put into the item.
  This maximum can not be increased after the knack is exercised.

  If the item is destroyed the Power points are released harmlessly into the surrounding area.

### Create Potion
+ **Traits:** Permanent
+ **Description:** Potions are liquids that store one or more Knacks.
  The Magnitude of the Create Potion knack needs to equal or exceed the highest Magnitude of the knack being stored into the potion.

  All potions are one use.
  They must be drunk in one swift gulp to work.

  The potion automatically works and doesn't incur a cost in power points to the person who is drinking it.

  The potion costs the enchanter power points.
  They must have the knack at the Magnitude enchanting at, with the power points of the knack being put into the potion.

  There is an associated cost of 1 Gold Ducat per Magnitude.

  To make the potion, the enchanter must roll successfully against Knackery for each knack being placed in the potion and against Lore (Potion Making).
  If they fail, the potion is ruined and they lose the cost of the ingredients.

  Potions take one hour per point of Magnitude of knack(s) stored to create.

  A potion must be stored in an air tight container, or it evaporates, losing one point of Magnitude per week.

### Cushion Fall
+ **Traits:** None.
+ **Description:** Each point of Magnitude of this knack eliminates one dice of falling damage for the recipient.

### Darkwall
+ **Traits:** Area 5, Magnitude 2, Non-Variable
+ **Description:** Light sources within a Darkwall area shed no light nad normal sight ceases to function.
  Other senses such as a bat's sonar and Night Vision (see [CH9](../9-creatures/README.md)) function normally.

  The caster may move the Darkwall 15 yards per Combat Round.
  If this option is chosen, the knack gains the Concentration trait.

### Demoralize
+ **Traits:** Magnitude 2, Non-Variable, Resist (Persistence)
+ **Description:** This knack creates doubt and uncertainty into the very heart and soul of the target.
  All close combat and ranged combat skill rolls for the target as Hard tests and the target may not cast offensive spells nor exercise offensive knacks.
  If this knack takes effect before combat begins the target will try to avoid fighting and will either run or surrender.
  The effects of this knack are automatically cancelled by the Fanaticism knack and vice versa.

### Detect X
+ **Traits:** Magnitude 1, Concentration, Non-Variable
+ **Description:** This covers a family of knacks that all operate in a similar fashion, allowing the character to locate the closest target of the spell within its range.
  This effect is stopped by a substances over a yard thick, such as castle walls or the ground between caves.
  It is also blocked by Countermagic, though the character will know the target is somewhere within range (though not its precise location) and that it is being protected by Countermagic.
  The separate Detect knacks are listed below nad each must be learned separately.
  + **Detect Enemy:** Gives the location of the nearest creatures that intend to harm the character.
  + **Detect Magic:** Gives the location of the nearest magic item, magical creature, or active spell/knack.
  + **Detect Species"** Each Detect Species knack will give the location of the nearest creature of the specified species.
  + **Detect Substance:** Each Detect Substance knack will give the location of the nearest substance of the specified type.

### Dispel Magic
+ **Traits:** Instant
+ **Description:** This knack will attack and eliminate active knacks and spells.
  Dispel Magic will eliminate a combined Magnitude of knacks and spells equal to its own Magnitude, starting with the most powerful affecting the target.
  If it fails to eliminate any spell or knack because the spell/knack's Magnitude is too high, then its effects immediately end and no more spells or knacks will be eliminated.
  Knacks and spells cannot be partially eliminated so a target under the effects of a spell whose Magnitude is higher than that of Dispel Magic will not have any spells or knacks currently affecting it eliminated.

### Disruption
+ **Traits:** Instant, Resist (Resilience)
+ **Description:** Disruption literally pulls a target's body apart.
  The target will suffer $$1\text{D}4$$ points of damage per point of Magnitude, ignoring any AP.

### Dragon's Breath
+ **Traits:** Instant, Magnitude 2, Non-Variable, Resist (Dodge)
+ **Description:** With this knack, the character spits a stream of fire at their target.
  If the fire is not dodged, it inflicts $$1\text{D}10$$ points of heat damage.
  Armor Points are effective against this damage and it counts as both magical and fire damage.

### Dull Weapon
+ **Traits:** None
+ **Description:** This knack can be exercised on any weapon.
  For every point of Magnitude it reduces the damage dealt by the target weapon by one.

### Enhance Skill (X)
+ **Traits:** None
+ **Description:** Like Detect X, this includes a number of different knacks, each of which affects a different non-combat skill.
  For each point of Magnitude, the recipient gains $$+10\%$$ to any skill test using the Enhanced skill.
  Alternatively, for each additional point of Magnitude of the knack, the character can effect one more target.
  The bonuses and targets can be split as necessary, providing each bonus in multiples of $$10\%$$ and the total of bonuses equals the knack's Magnitude $$\times 10\%$$

### Extinguish
+ **Traits:** Instant
+ **Description:** This knack instantly puts out fires.
  At Magnitude 1 it can extinguish a flame, at Magnitude 2 a small fire, at Magnitude 3 a large fire, and at Magnitude 4 will put out an inferno.

### Extra Defense
+ **Traits:** Ranged
+ **Description:** Each point of Magnitude allows the target to make one extra close combat defensive reaction per combat round.

### Fanaticism
+ **Traits:** Magnitude 2, Non-Variable
+ **Description:** The target of this knack will have their close combat increased by $$25\%$$

### Farsight
+ **Traits:** Concentration
+ **Description:** Each Magnitude of this Knack extends the character's field of vision by ten yards as long as they maintain their concentration.
  Although they can see small details at a distance, this spell does not let the character see through walls or other obstructions.

### Firearrow
+ **Traits:** Touch, Trigger, Magnitude 2,  Non-Variable
+ **Description:** Exercising this knack on a missile will cause it to burst into flame when it is fired and strikes a target.
  When it hits a target, the missile will deal $$1\text{D}10$$ points of magical fire, damage instead of its normal damage.
  Since Firearrow does magical damage, it affects creatures that are immune to normal damage.
  A missile under the effects of Firearrow cannot benefit from Speedart.

### Fireblade
+ **Traits:** Touch, Magnitude 4, Non-Variable
+ **Description:** For the duration of the spell, the target weapon will deal $$1\text{D}10$$ magical fire damage instead of its normal damage.
  A weapon under the effects of Fireblade cannot benefit from Bladesharp.
  Since Fireblade does magical damage, it damages creatures immune to normal damage.

### Fist of Gold
+ **Traits:** None
+ **Description:** This knack creates a minor illusion of $$1\text{D}10$$ Gold Ducats per level of Magnitude that persists for the duration of the knack.

### Fist of the Wind
+ **Traits:** Instant
+ **Description:** Each point of Magnitude allows the caster to make one extra unarmed close combat attack or grapple.

### Flying Kick
+ **Traits:** Magnitude 2, Non-Variable
+ **Description:** This knack allows the recipient to make a Normal move as a flying leap through the air, then make a Kick attack at the end of the move.

### Frostbite
+ **Traits:** Instant, Magnitude 2, Non-Variable, Resist (Resilience)
+ **Description:** This attack knack allows the character to freeze his opponent, dealing $$1\text{D}8$$ points of damage, ignoring any AP.
  Magical damage that protect against cold damage can block this effect but mundane items (such as cold weather gear) are ineffective.

### Glue
+ **Traits:** Touch, Area
+ **Description:** This knack covers an area of one inch square for each Magnitude with extremely sticky glue.
  If a creature steps on the glue, it must make an Athletics roll vs the $$\text{Magnitude} \times 10\%$$ to avoid being stuck for one round.
  On subsequent rounds it  must make the same roll to break free.
  This knack can also be used for more conventional repairs, a broken sword for example, with $$\text{Magnitude} \times 10\%$$ being the chance that the item won't break again, if used in circumstances that would cause it to.

### Hand of Death
+ **Traits:** Touch, Instant, Magnitude 4, Non-Variable, Resist (Resilience)
+ **Description:** This fearsome knack allows the character to deal an awful wound with the merest touch.
  Casting the _Hand of Death_ charges the character with the knack.
  Touching an unsuspecting target, or succeeding at an Unarmed attack against a wary target, releases the knack's effect.
  If the Resilience test to resist the effect is failed, the victim immediately loses half their maximum Hit Points, and suffers a a Major Wound (if those rules are being used).
  If the Resilience test is a success, the target only loses $$1\text{D}3$$ Hit Points. Armour does not protect against this damage.

### Harden
+ **Traits:** Touch, Magnitude 1, Non-Variable
+ **Description:** This knack makes a target item unbreakable for the duration of the knack.
  Therefore weapons with this knack exercised on them will not break when a Fumble is rolled in combat, and it allows items that are normally too brittle to be wielded in combat to be used as improvised weapons.

### Heal
+ **Traits:** Touch, Instant, Magnitude 2
+ **Description:** For every two points of Magnitude, the character can repair one Hit Point to damage of either themself or another target, but only if the character has sufficient understanding of the damage and what's required to heal the injured character.
  A Magnitude 6 or higher Heal knack will also re-attach a severed limb if cast within ten rounds of the loss.
  A Magnitude 5 or higher Heal knack will also cure any single poison or disease affecting the target, but this does not negate the requirement to understand the disease / poison.

  In effect, the limitation on this knack prevents a character from healing another creature of an injury or disease they do not understand.
  If a character succeeds on a Lore (Medicine) test, they understand the injury or disease well enough to heal it.

### Hinder Skill (X)
+ **Traits:** Ranged, Resist (Persistence)
+ **Description:** This is a number of different knacks, each of which affects a different skill.
  For each point of Magnitude of the knack, the target gains a $$-10\%$$ penalty to the next skill test using the affected skill.

  Alternatively, for each additional point of Magnitude of the knack, the caster can affect one more target.
  The bonuses and targets can be split as necessary providing each penalty is in multiples of $$10\%$$ and the total of bonuses equals the spells $$\text{Magnitude} \times 10\%$$.
  If used in this way, each target is affected separately; if one target succeeds on resisting the knack, other targets may fail and be affected.

### Ignite
+ **Traits:** Instant, Magnitude 1, Non-Variable
+ **Description:** This knack will set fire to anything flammable within range, creating a flame.
  Skin or flesh cannot be ignited and if the target is attached to a living being (such as hair, fur or clothes) then the spell gains the Resist (Resilience) trait.

### Invisibility
+ **Traits:** Personal, Touch, Concentration, Magnitude 4, Non-Variable
+ **Description:** For the duration of the knack the recipient is completely invisible to sight.
They can still be heard, felt or smelled, with a $$-25\%$$ to Perception tests.
Also, the knack is automatically dispelled if the character loses concentration, or the recipient casts a spell, exercises a knack, or makes an attack.
The recipient also becomes visible immediately after the knack ending, so even if the character immediately exercises another Invisibility knack there will be a delay between exercises where the recipient is visible.

### Ironmind
+ **Traits:** Touch
+ **Description:** This knack hardens the resolve of the character that it is exercised upon for its duration.
  Each level of Magnitude of the knack adds $$10\%$$ to all Persistence tests against magical attacks to the mind (e.g. Fear, Befuddle etc.) or opposed tests vs Influence.

### Knockback
+ **Traits:** Instant, Resist (Resilience)
+ **Description:** The target of this knack is knocked back a number of yards equal to the knack's magnitude.

### Knockdown
+ **Traits:** Instant, Magnitude 2, Non-Variable, Resist (Resilience)
+ **Description:** The target of this knock is knocked down prone.

### Leap
+ **Traits:** Touch, Resist (Dodge)
+ **Description:** This knack causes the target to leap 2y up in the air for each point of Magnitude.
If cast upon an unwilling target who fails their resistance roll, they will then fall to earth taking normal falling damage (see [CH8](../8-Questing/README.md)).

### Light
+ **Traits:** Magnitude 1, Non-Variable, Area 10
+ **Description:** Exercised on a physical object (including living material), this knack causes the object to shed light across the area of effect.
  Note that only the specified area is illuminated – everything outside the area of effect is not.
  This knack creates raw light, not a flame.

### Mindspeech
+ **Traits:** None
+ **Description:** This knack can affect one target for every point of Magnitude, allowing telepathy between the character and any target, though targets will not have telepathy with one another.
  The words transmitted by telepathy must be whispered and will be heard directly in the head of the recipient, in the same language in which it was spoken. 

### Mobility
+ **Traits:** None
+ **Description:** For every point of Magnitude of this knack the target's Movement rate will be increased by 2 yards.

### Multi-attack
+ **Traits:** Instant
+ **Description:** Each point of Magnitude allows the character to make one extra close-combat attack.

### Personal Insight
+ **Traits:** Magnitude 2, Non-Variable
+ **Description:** This knack gives the caster or recipient a very direct insight into a small question directly relevant to them, in the form of an internal intuition upon a successful Knackery test.
For example the question 'Why can I not harm the creature?' would get the answer 'Because your sword is not enchanted', while 'Why can we not harm the creature?' would not get an answer. 

### Pierce
+ **Traits:** Touch
+ **Description:** This knack can be exercised on any weapon with a blade or point.
  For every point of Magnitude, it ignores one armour point when it strikes armour.
  Pierce can bypass magical armour as easily as normal armour.

### Protection
+ **Traits:** None
+ **Description:** For every point of Magnitude of this knack one armour point is added to the armour of the target.
  This stacks with any existing armour and is treated in the same way.

### Read Emotion
+ **Traits:** Instant, Magnitude 1, Non-Variable, Resist (Persistence)
+ **Description:** When exercised, this knack tells you what the true emotional state of the target is if they fail a Persistence roll.

### Resist Element
+ **Traits:** None
+ **Description:** This knack increases Resistance against hostile effects, magic or otherwise, from a given element (Air/Darkness/Earth/Fire/Water) by $$10\%$$ per Magnitude, and subtracts 1 point of damage from that element per Magnitude.

### Restore Energy
+ **Traits:** Touch, Instant
+ **Description:** Each point of this knack's Magnitude instantly restores one fatigue level to the recipient.

### Sap Energy
+ **Traits:** Touch, Instant, Resist (Resilience)
+ **Description:** Each point of this knack's Magnitude instantly inflicts one fatigue level on the recipient.

### Scare
+ **Traits:** Magnitude 2, Non-Variable, Resist (Persistence) 
+ **Description:** The target is scared for $$1\text{D}6$$ rounds.
  Scared targets must withdraw from combat with the character for the duration of the knack, and move as quickly as they are able, directly away from the character.

### Second Sight
+ **Traits:** Magnitude 3, Non-Variable
+ **Description:** Second Sight allows the character to gauge the CHA of every creature and magic item within range.
  The knack is blocked by anything that blocks normal vision.
  The character will know if each aura created by the illuminated CHA is less than their own CHA, within three points of their own CHA or greater than their own CHA.

  Additionally, Second Sight provides a $$+25\%$$ bonus on Perception tests to notice hidden magical items or hiding people or creatures.
  Second Sight will also reveal invisible entities; though only a hazy image will show (treat such targets as partially obscured).

### Skybolt
+ **Traits:** Instant, Magnitude 3, Non-Variable, Resist (Dodge)
+ **Description:** The character summons a lightning bolt from the heavens regardless of the weather.
  The target must be outdoors in plain view.
  Skybolt inflicts $$2\text{D}6$$ points of damage to a single chosen target.
  Only magical Armour Points offer protection against this damage

### Slow
+ **Traits:** Resist (Resilience)
+ **Description:** For every point of Magnitude of this knack the target's Movement Rate will be decreased by 2y.
A target's Movement may not be reduced to below one yard through use of this knack.

### Speedart
+ **Traits:** Touch, Trigger, Magnitude 2, Non-Variable
+ **Description:** Exercised on a missile this knack is triggered when it is fired.
  It gives a $$+25\%$$ to Ranged Combat and $$+3$$ damage while using the missile.
  A missile under the effects of Speedart cannot benefit from Firearrow.

### Strength
+ **Traits:** 
+ **Description:** For every point of Magnitude of this knack, the target's Damage increases by $$+1$$ and strength based athletics tests are $$+10\%$$ per Magnitude.
  Note the Damage increase is not treated as magical damage. 

### Talk to Animal
+ **Traits:** Magnitude 3, Non-Variable
+ **Description:** With this knack the recipient is able to talk to any beast within ten yards of them.
  This communication is verbal, therefore the recipient must be able to speak and be heard by the target animal.  

### Thunder's Voice
+ **Traits:** None
+ **Description:** For every point of Magnitude of this knack, the caster has $$+10\%$$ added to their Influence skill and can also be heard at up to the spell's $$\text{Magnitude} \times 100$$ in yards.

### Tongues
+ **Traits:** Magnitude 2, Non-Variable
+ **Description:** This spell allows the recipient to speak another language perfectly for its duration.

### Unlock
+ **Traits:** Touch, Instant
+ **Description:** This knack has a chance of opening a lock equal to the knack's $$\text{Magnitude} \times 20\%$$, minus any modifiers due to the intricacy of the lock.

### Vigor
+ **Traits:** Touch
+ **Description:** For every point of Magnitude of this knack, the target's Hit Points score increases by +2.
  A target cannot have its Hit Points increased in this way to more than twice its original score.
  Damage is taken from the ‘magical' Hit Points first, so when the spell dissipates the damage that was inflicted on the magical Hit Points disappear too.
  Recalculate the Major Wound level for the character while the knack is in effect. 

### Vomit
+ **Traits:** Ranged, Resist (Resilience)
+ **Description:** This knack incapacitates its Victim for 1 round per point of Magnitude, due to uncontrollable vomiting.
  On a fumbled resilience roll the Victim takes $$1\text{D}6$$ Hit Points damage. 

### Walk On (Element)
+ **Traits:** Magnitude 3
+ **Description:** This knack allows the recipient to walk on the specified element (Air/Darkness/Earth/Fire/Water) without sinking or taking any harm from what is being walked on for the knack's duration.
  With this knack for the appropriate element, the character can walk across lava, quicksand, water, or even through the air.
  Each additional point of Magnitude increases the duration of the knack by 1 minute. 

### Water Breath
+ **Traits:** Touch
+ **Description:** This knack allows the target to breathe water for the duration of the knack.
  For every point of Magnitude, one additional person can be included in the knack, or increase the duration by one minute.
  Water Breath has no effect on the target's ability to breathe air.

### Weapon Enhance
+ **Traits:** Touch
+ **Description:** This knack can be cast on any close combat weapon or any unarmed attack.
  For every point of Magnitude, it increases the chance to hit with the weapon by +10% and deals one point of extra damage.
  This extra damage is magical and will affect creatures that can only be hurt by magic.
  The weapon's base damage remains non-magical.
  A weapon under the effects of this knack cannot benefit from Fireblade.