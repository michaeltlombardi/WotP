# Disease  
Disease is a source of threat in Tephra, often made worse by the infusion of magic in the world.
Every type of disease has the following information detailed:  
+ **Name:** The disease's name.
  Also, if the disease is magical in nature, it will be mentioned here.  
+ **Type:** Lists whether the disease is spread through contamination, ingestion, injury, touch or is airborne.  
+ **Delay:** The time between the diseases introduction to a character, to the time its effect takes hold.
  It is also the time following disease contraction that a victim will be forced to make follow-up opposed disease tests.  
+ **Potency:** This is a number between 10 and 100 that measures the strength of a disease.
  Some magical diseases, like the shining plague, have even higher Potencies.
  A character must make an opposed Resilience test versus the disease's Potency test in order to avoid or mitigate the damage of the disease.  
+ **Effect:** Usually hit point damage, though this is not universal.
  Many diseases will apply a penalty to Characteristics or skills.
  More exotic diseases may cause hallucinogenic effects, paralysis or a combination of effects.
  These take place after the delay noted above.  

The effects of the disease cannot be removed or healed until the disease itself has been neutralised or has dissipated in the victim's system.
Hit point damage caused by disease will not automatically heal – it must be healed through magical or natural healing.  

Unlike a poison, diseases will progress if a character does not resist its effects.
Once the first opposed test is failed by the victim, they will have to make an additional opposed test (after an amount of time determined by the disease's delay statistic).  

If the victim succeeds this second opposed test, he has overcome the worst of the disease and will no longer suffer its effects, other than remaining hit point damage, after a while.
Use the disease's delay statistic to determine how long this takes.  

If the victim fails this second opposed test, he falls deeper into the disease.
Apply all of the disease's effects again to the character.
Once the delay period has elapsed once more, the victim will have to make a third opposed disease test, and so on.  

### Disease Succeeds, Character Fails  
If the disease succeeds its Potency test and the character fails his Resilience test, the disease has its full effect.  
### Character Succeeds, Disease Fails  
If the character succeeds his Resilience test and the disease fails its Potency test, the disease has no effect.  
### Both Disease and Character Succeed  
Whoever rolled the highest in their test wins.  
### Both Disease and Character Fail  
Whoever rolled the lowest in their test wins.  

## Disease List
The following list of diseases is _non-exhaustive_.

| Disease         | Type          |  Delay               | Potency  | Effect |
|:---------------:|:-------------:|:--------------------:|:--------:|:-------|
| Dysentery       | Ingestion     | $$1\text{D}4$$       | $$40\%$$ | Fatigued for duration, $$-1$$ $$\text{BOD}$$
| Leprosy         | Touch         | $$5\text{D}4$$ years | $$20\%$$ | All Resilience tests are Hard, cannot benefit from natural healing.
| Malaria         | Injury        | $$2\text{D}6$$ days  | $$30\%$$ | Fatigued for duration, complications on successive failures.
| Melting Disease | Contamination | Immediate            | $$25\%$$ | $$-2$$ $$\text{BOD}$$ and $$\text{DEX}$$ per day
| Rabies          | Injury        | $$2\text{D}6$$ weeks | $$95\%$$ | Hydrophobia, aggression, $$-2$$ to all characteristics each day.
| The Shakes      | Touch         | $$1\text{D}4$$ days  | $$50\%$$ | $$\frac{1}{2} \text{DEX}$$ for duration, $$1\text{D}6$$ damage per day
| Tetanus         | Injury        | $$2\text{D}4$$ days  | $$40\%$$ | $$1\text{D}4$$ damage and $$-1$$ $$\text{DEX}$$ per day.
| Tuberculosis    | Airborne      | $$1\text{D}4$$ days  | $$35\%$$ | $$1\text{D}6$$ damage and $$-1$$ $$\text{BOD}$$ per day.
| Typhoid Fever   | Ingestion     | $$1\text{D}6$$ days  | $$40\%$$ | Fatigued for duration
| War's Gift      | Touch         | $$2\text{D}6$$ days  | $$35\%$$ | Fatigued and light sensitive for duration and $$-1$$ BOD, INT, and REA per day.

<!-- Block-Breaking Comment to allow adjacent alert blocks -->
<!--
> **[danger] Disease Name**
>
> _Description of disease_
> + **Type:** 
> + **Delay:** 
> + **Potency:** 
> + **Effect:** 
-->

<!-- Block-Breaking Comment to allow adjacent alert blocks -->
> **[danger] Dysentery**
>
> _Bloody diarrhea, fever, and abdominal pain causing the victim to weaken and possibly die._
> + **Type:** Contamination
> + **Delay:** 1-3 days
> + **Potency:** $$40\%$$
> + **Effect:** After onset and for the duration of infection the victim is fatigued.
>   Also for each day that the victim suffers from Dysentery their $$\text{BOD}$$ is reduced by $$1$$.

<!-- Block-Breaking Comment to allow adjacent alert blocks -->
> **[danger] Leprosy**
>
> _This difficult-to-contract disease is spread by contact with another victim and causes fatigue, sores, skin lesions, nasal congestion, and prevents wounds from healing._
> + **Type:** Touch
> + **Delay:** $$5\text{D}4$$ years
> + **Potency:** $$20\%$$
> + **Effect:** After onset and for the duration all resilience tests are Hard for the victim.
>   Additionally, for the duration, the victim cannot benefit from natural healing.

<!-- Block-Breaking Comment to allow adjacent alert blocks -->
> **[danger] Malaria**
>
> _This disease is spread by bites from infected mosquitos; while not incredibly dangerous on its own, malaria can introduce severe complications._
> + **Type:** Injury
> + **Delay:** $$2\text{D}6$$ days
> + **Potency:** $$30\%$$
> + **Effect:** Victim is fatigued for the duration.
>   On successive failures against the disease the character endures a new complication:
>
> | Roll $$1\text{D}6$$ | Complication |
|:---------------------:|:------------ |
| 1                     | Unable to eat, begin to starve
| 2                     | Kidney failure, $$-2$$ $$\text{BOD}$$ per day
| 3                     | Circulatory shock, $$-1\text{D}4$$ $$\text{BOD}$$ per day
| 4                     | Blackwater Fever, $$-1$$ $$\text{BOD}$$ per day
| 5                     | Cerebral malaria, $$-1$$ $$\text{REA}$$ per day
| 6                     | Increased onset: Delay becomes $$1\text{D}4$$ days

<!-- Block-Breaking Comment to allow adjacent alert blocks -->
> **[danger] Melting Disease**
>
> _This disease only occurs in areas where there have been large discharges of magic, such as when two powerful Crafters have been fighting each other and caused the strands of reality to temporarily bend and warp._
> + **Type:** Contamination.
> + **Delay:** Immediate
> + **Potency:** 25
> + **Effect:** The victim starts melting, losing two points of BOD and DEX per day.

<!-- Block-Breaking Comment to allow adjacent alert blocks -->
> **[danger] Rabies**
>
> _Spread by the bites of infected creatures, this disease destroys the victims mind and body causing them to go mad before their end; it is nearly always fatal once contracted._
> + **Type:** Injury
> + **Delay:** $$2\text{D}6$$ weeks
> + **Potency:** $$95\%$$
> + **Effect:** After onset and for the duration the victim is subject to hydrophobia and aggression.
>   The victim must make a contested Charisma (Persistence) test against the disease's potency whenever faced with water, drinking, or whenever a situation is presented that would normally have annoyed or inconvenienced the victim before infection.
>   Failure causes the victim to lash out violently at those nearby.
>   Additionally, once symptoms present the victim's characteristics all begin to degrade, being reduced by $$2$$ each day.

<!-- Block-Breaking Comment to allow adjacent alert blocks -->
> **[danger] The Shakes**
>
> _This flu like disease renders its victims in a cold and constantly shaking state._
> + **Type:** Touch
> + **Delay:** 1-2 days
> + **Potency:** $$50\%$$
> + **Effect:** After onset and for duration, $$\text{DEX}$$ is halved.
>   Also for each day that the victim suffers from the Shakes they take $$1\text{D}6$$ hit points of damage.

<!-- Block-Breaking Comment to allow adjacent alert blocks -->
> **[danger] Tetanus**
>
> _This disease, contracted by getting contaminants in an open wound, causes terrible spasms, fevers, and headaches._
> + **Type:** Injury
> + **Delay:** $$2\text{D}4$$ days
> + **Potency:** $$40\%$$
> + **Effect:** $$1\text{D}4$$ damage and $$-1$$ $$\text{DEX}$$ per day.

<!-- Block-Breaking Comment to allow adjacent alert blocks -->
> **[danger] Tuberculosis**
>
> _This disease is spread by the coughing of other infected victims and causes those infected to cough blood, lose weight, and suffer from fevers._
> + **Type:** Airborne
> + **Delay:** $$1\text{D}4$$ days
> + **Potency:** $$35\%$$
> + **Effect:** $$1\text{D}6$$ damage and $$-1$$ $$\text{BOD}$$ per day.

<!-- Block-Breaking Comment to allow adjacent alert blocks -->
> **[danger] Typhoid Fever**
>
> _This disease is spread by poor sanitation practices and causes the subject to suffer from fevers, abdominal pain, and lack of energy._
> + **Type:** Ingestion
> + **Delay:** $$1\text{D}6$$ days
> + **Potency:** $$40\%$$
> + **Effect:** Fatigued for duration.

<!-- Block-Breaking Comment to allow adjacent alert blocks -->
> **[danger] War's Gift (Typhus)**
>
> _Typhus, known in Tephra as War's Gift, is a disease spread by infected lice and thus through contact with other infected victims, and manifests with a rash, severe headaches, pain, fever, cough, stupor, light sensitivity, and delirium._
> + **Type:** Touch
> + **Delay:** $$2\text{D}6$$ days
> + **Potency:** $$35\%$$
> + **Effect:** The victim is fatigued and light sensitive for the duration.
>   Additionally, the victim loses one BOD, INT, and REA each day.