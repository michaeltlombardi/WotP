# Miracles
Before a miracle can be performed, the following process must be followed:

In order to acquire a particular Miracle, the character must possess the Supplication (Organization) skill appropriate to his organization and be of Supplicant, Champion, or Principal status within that organization.

The character must pay a cost in Improvement Points, equal to twice the Magnitude of the miracle, to the Organization.
This may be done in an incremental fashion, i.e. the player buys Shield 1 for two Improvement Points and then later increases this to Shield 3, by spending an additional four points.
These points are not regained, even when the character leaves the organization.

## Performing Miracles
A character must be able to gesture with his hands and be able to chant in order to perform a miracle.
Whenever a miracle is performed, there will always be a sight and sound that nearby creatures can detect, be it a flash of light, a crack of thunder or a shimmering in the air.
The exact effects, are up to the GM and Player to decide but will automatically be detected by any creatures within ten times the Magnitude of the miracle, in yards.

Performing a Miracle is automatically successful.
No dice need be rolled, no chances of a fumble or critical either.

### Power Points
Miracles do not cost any Power Points when they are performed.
### Performance Time
Miracles always take only a single combat Action to perform.
### Perform Once Only
Each Miracle may be performed only once, after which the character must make contact with a Grantor of the organization and petition them for more power (making a Supplication test; failure causes the Supplicant to regain only half their missing Miracles and requires them to perform a quest for the Grantor before the other half are returned.
The Character need not spend Improvement Points again.
### Limitations
Miracles do not stack, i.e. Shield 1 + Shield 2 does not equal Shield 3.
 ### Dismissing Miracles
A character can dismiss any Permanent or Duration Miracle(s) he has performed as a single combat action.
Ceasing to perform a Concentration Miracle is immediate and not a Combat Action.
### Splitting Magnitude
Miracles allows the Supplicant to ‘split' a miracle's Magnitude into multiple Miracles.
For instance, if the character knows the Absorption Miracle at Magnitude 3, they may choose to perform it as a single Magnitude 3 Miracle, or may split it into three Magnitude 1 Absorption Miracles, or one Magnitude 1 and one Magnitude 2 Absorption Miracle.
The split Miracles are treated as separate instances and are performed separately.
### The Power of Miracles
When in a direct contest with either knacks or dweomers, Miracles are considered to have double their normal Magnitude.
### Common Miracles
The following miracles are listed as ‘All', since all organizations teach them:
Consecrate, Create Blessed Item, Create Idol, Dismiss Magic, Divination, Excommunicate, Exorcism, Extension, Find X,  Mindlink, Soul Sight, Spirit Block, Spiritual Journey.

## Regaining Miracles
Unlike knackery, which relies on Power Points directly to exercise knacks, or Crafting, which requires dangerous use of magical energy to apply dweomers but does not require regaining points of any sort, Miracles are hard to come by.

Supplicants can regain Miracles in one of two ways:

### 1. Visit an Organizational Sanctum Special Event
Most organizations will have special events - holy days, annual galas, quarterly celebrations, etc.
A character attending these events in good standing can make a supplication roll - success will entirely replenish their Miracles.

### 2. Fulfill Organizational Goal
For supplicants who perform miracles more frequently, they will find that using miracles in the course of furthering the organization is the best means of being able to continue to perform miracles.

As well as gaining Improvement Points for performing Organizational Duties, miracles are instantly regained.
Minor deeds regain one-two point miracles, while Major deeds regain miracles that cost three points, or more.∏tri

## Miracle Traits and Descriptions
The traits used by Divine Magic miracles are detailed below.
+ **Area (X):** The miracle affects all targets within a radius specified in yards.
+ **Concentration:** The miracle's effects will remain in place so long as the character concentrates on it.
  Concentrating on a miracle is functionally identical to performing the miracle, requiring the character to continue to gesture with both arms, chant, and ignore distractions.
+ **Duration (X):** The miracle's effects will stay in place for the number of minutes indicated.
+ **Instant:** The miracle's effects take place instantly.
  The miracle itself then disappears.
+ **Magnitude (X):** The strength and power of the miracle.
+ **Permanent:** The miracle's effects remain in place until they are dispelled or dismissed.
+ **Progressive:** This indicates that the miracle can be acquired and performed at greater levels of Magnitude than the minimum.
+ **Ranged:** Ranged miracles may be cast upon targets up to a maximum distance of the character's CHA x 5 in yards.
+ **Resist (Dodge/Persistence/Resilience):** The miracle's effects do not take effect automatically.
  The target may make a Dodge, Persistence or Resilience test (as specified by the miracle) in order to avoid the effect of the miracle entirely.
  Note that Resist (Dodge) miracles require the target to be able to use Reactions in order to Dodge.
  In the case of Area miracles, the Resist (Dodge) trait requires the target to dive in order to mitigate the miracle's effect.
+ **Touch:** Touch miracles require the character to actually touch their target for the miracle to take effect.
  The character must remain in physical contact with the target for the entire performing.
+ **Organization:** The type of organization that offers this miracle to it's worshippers.
  If the organization is listed as ‘All', the miracle is a Common miracle available in all organizations.
  The organization's description will help determine which miracles should or should not be available.

## List of Miracles
| Miracle                                       | Traits                                                             | Organizations | Description |
|:---------------------------------------------:|:-------------------------------------------------------------------|:-------------:|:------------|
| [Absorption](#absorption)                     | Touch, Progressive, Duration 15, Magnitude 1                       |               | Absorbs incoming dweomers, knacks, and miracles aimed at the target or their equipment, converting their magical energy into Power Points which are then available to the target.
| [Berserk](#berserk)                           | Touch, Duration 15, Magnitude 2                                    |               | The recipient is overcome with bloodlust, causing them to disregard their own safety and loyalties but imbuing them with tremendous stamina and combat ability.
| [Block Fertility](#block-fertility)           | Permanent, Magnitude 3                                             |               | Makes the target infertile
| [Breathe Water](#breathe-water)               | Touch, Duration 15, Magnitude 2                                    |               | Allows an air-breathing creature to breathe water for the spell's duration.
| [Consecrate](#consecrate)                     | Area, Special Duration , Progressive, Permanent, Magnitude 1       |               | The ritual that creates an empowered sanctum
| [Create Blessed Items](#create-blessed-items) | Area, Special Duration , Progressive, Permanent, Magnitude 1       |               | Allows the character to create items which store Miracles that are imbued by a Grantor with power from the organization.
| [Create Idol](#create-idol)                   | Magnitude 4                                                        |               | Creates a magical idol of the organization.
| [Dismiss Magic](#dismiss-magic)               | Ranged, Instant, Progressive, Magnitude 1                          |               | Dismiss Magic will eliminate a combined Magnitude of Miracles, Dweomers, and Knacks equal to its own Magnitude, starting with the most powerful affecting the target.
| [Divination](#divination)                     | Instant, Progressive, Magnitude 1                                  |               | For each point of Magnitude the character can ask one question of their Grantor, which they must answer to the best of their ability.
| [Enhance Fertility](#enhance-fertility)       | Permanent, Magnitude 3                                             |               | Cures infertility and makes fertile creatures over fertile.
| [Enhance Unit](#enhance-unit)                 | Duration 6 hours, Magnitude 3                                      |               | Members of a millitary unit gains $$+1\text{D}6$$ and are immune to routing for the duration of this miracle.
| [Excommunicate](#excommunicate)               | Permanent, Magnitude 5, Resist (Persistence)                       |               | Severs the mystical link a Supplicant enjoys with their organization, causing the target of the miracle to immediately and permanently lose all miracles and knacks from the character's cult.
| [Extension](#extension)                       | Progressive, Duration Special, Magnitude 1                         |               | Lengthens the duration of any miracle with the Duration trait
| [Fear](#fear)                                 | Ranged, Instant, Magnitude 1, Resist (Persistence)                 |               | Causes the target to be gripped with overwhelming fear
| [Find X](#find-x)                             | Ranged, Duration 15, Magnitude 1                                   |               | Finds substance or people, dependent on what X is.
| [Illusion](#illusion)                         | Ranged, Duration 15, Progressive, Magnitude 1                      |               | Creates an illusion based on all five senses.
| [Jigsaw](#jigsaw)                             | Duration 6 hours, Magnitude 4                                      |               | Causes the victim to literally fall to pieces.
| [Lightning Strike](#lightning-strike)         | Ranged, Instant, Progressive, Magnitude 1, Resist (Dodge)          |               | Creates a lightning bolt which does $$1\text{D}6$$ per Magnitude.
| [Madness](#madness)                           | Ranged, Instant, Magnitude 1, Resist (Persistence)                 |               | Causes the target to lose contact with reality and become a gibbering loon.
| [Mindblast](#mindblast)                       | Ranged, Instant, Progressive, Magnitude 1, Resist (Persistence)    |               | Applies a penalty to the victim's REA equal to the Magnitude of the spell.
| [Mindlink](#mindlink)                         | Ranged Duration 15, Progressive, Magnitude 1                       |               | Allows the transmission of conscious thoughts, spell knowledge and Power Points between one participant per Magnitude.
| [Miraculous Item](#miraculous-item)           | Duration 1 Hour, Magnitude Variable                                |               | Creates an item from nowhere.
| [Puppet](#puppet)                             | Duration 6 hours, Concentration, Magnitude 3, Resist (Persistence) |               | Creates a puppet which can be used to control the spell's victim.
| [Quicksand](#quicksand)                       | Area, Magnitude 2                                                  |               | Causes a selected area of ground to become quicksand.
| [Radiant Appearance](#radiant-appearance)     | Duration 1 Day, Magnitude 2                                        |               | +50% Influence, light hating creatures -25% vs recipient.
| [Reflection](#reflection)                     | Ranged, Duration 15, Progressive, Magnitude 1                      |               | Reflects incoming spells weaker or equal to its Magnitude.
| [Repair and Replace](#repair-and-replace)     | Instant, Magnitude Variable                                        |               | Instantly repairs broken items.
| [Rout](#rout)                                 | Magnitude 3                                                        |               | When cast at an enemy unit causes it to flee in panic.
| [See Past](#see-past)                         | Area, Concentration, Magnitude 2                                   |               | Allows the character to look into an area's past.
| [Shield](#shield)                             | Duration 15, Progressive, Magnitude 1                              |               | +1 AP, +10% vs Magical attacks per Magnitude.
| [Soul Sight](#soul-sight)                     | Touch, Duration 15, Magnitude 1                                    |               | Allows the character to see another's Aura and determine CHA and PP.
| [Sunspear](#sunspear)                         | Ranged, Instant, Magnitude 4, Resist (Dodge)                       |               | A spear of light that does $$4\text{D}6$$ damage
| [Sun Disc](#sun-disc)                         | Ranged, Magnitude 1, Resist (Dodge)                                |               | A blinding disc of light, dodge or be blind for $$1\text{D}4$$ hours
| [Sureshot](#sureshot)                         | Ranged, Duration 15, Magnitude 1                                   |               | Automatic hit with missile weapon
| [Touch of Death](#touch-of-death)             | Touch, Instant, Magnitude 4, Resist (Persistence)                  |               | Kills the victim on failed persistence roll
| [Treasury](#treasury)                         | Duration 1 Day, Magnitude 4                                        |               | Creates a locked and alarmed room
| [True (Weapon)](#true-weapon)                 | Ranged, Duration 15, Magnitude 3                                   |               | Doubles damage
| [Ward Camp](#ward-camp)                       | Area, Duration 8 hours, Magnitude 2                                |               | Protects your camp against unwelcome intruders.
| [Wax Effigy](#wax-effigy)                     | Magnitude 4, Resist (Persistence)                                  |               | This spell enchants a small wax representation of the intended victim.
| [Whirlwind](#whirlwind)                       | Duration 15, Variable                                              |               | Carries targets up, up and away in their own personal Whirlwind!

### Absorption
Touch, Progressive, Duration 15, Magnitude 1

This spell absorbs incoming spells aimed at the target or his equipment, converting their magical energy into Magic Points which are then available to the target.
Once cast on a subject, Absorption will attempt to absorb the effects of any spells cast at the target.
It will not have any effect on spells that are already affecting a character.
The effects of Absorption depend on the relative Magnitude of both itself and the incoming spell – see the Absorption Results table for more details.
Any spell absorbed by this spell is cancelled and has no effect.

> **[info] Absorption Results**
>
> | Incoming Magnitude is...                     | Effect |
|:----------------------------------------------:|:-------|
| Equal to or weaker than Absorption's Magnitude | Incoming spell absorbed and Absorption remains.
| Stronger than Absorption's Magnitude           | Absorption eliminated and incoming spell takes effect.

A character may not accumulate more Power Points than their CHA while Absorption is in effect – excess Power Points garnered through Absorption simply vanish.
Absorption is incompatible with Reflection and Shield.

### Berserk
Touch, Duration 15, Magnitude 2

The recipient of this miracle is overcome with bloodlust, causing them to disregard their own safety and loyalties but imbuing them with tremendous stamina and combat ability.

The recipient will automatically succeed any Resilience test for the duration of the miracle.
The recipient also automatically succeeds at any Fatigue tests and cannot be rendered unconscious.
The Close Combat tests of the recipient are easy for the miracle's duration.

However, the subject may not Parry, Dodge or apply any dweomers while under the influence of Berserk.
Normally, the recipient remains in the Berserk state for the entire 15 minute duration of the miracle, but Games Masters may allow a Berserk character to shake off the effects with a hard Persistence test.
At the end of the miracle, the recipient immediately becomes Fatigued.

Berserk may not be combined with Fanaticism – Berserk will always take precedence in such cases.

### Block Fertility
Permanent, Magnitude 3

While this miracle is in place, the recipient is unable to conceive.
This can be seen as a blessing or a curse depending on the view of the recipient.
The miracle can dispelled by the caster whenever they want.
Otherwise the effects of the miracle is permanent.

### Breathe Water
Touch, Duration 15, Magnitude 2

This miracle allows an air-breathing creature to breathe water for the miracle's duration (the subject will still be able to breathe air as well).
It may also be used upon a water-breathing creature to allow it to breathe air.

### Consecrate
Area, Special Duration , Progressive, Permanent, Magnitude 1

This miracle is as much a part of a temple's foundation as is its cornerstone, but may actually be cast almost anywhere.
It creates a sphere with a radius of ten yards per point of Magnitude.
The consecrated sphere is sacred to the performer's organization.
Consecrate by itself does nothing to keep outsiders at bay, but the performer of the miracle will know immediately if a miracle, spell or someone who is not a lay member of his organization crosses the boundaries of the Consecrate miracle.

### Create Blessed items
Area, Special Duration , Progressive, Permanent, Magnitude 1

This miracle allows the character to create items which store Divine Magic miracle(s) that are blessed by a Priest with power from the cult's Deity.

Only Principals and Champions can create blessed items.

The enchanter forgets the miracle(s) he blesses the item with.

The wielder of the item can perform the miracle(s) the item is blessed with, but once the item's power is discharged then it must be reconsecrated at a sanctum of the organization.
The consecration takes as many hours as the Magnitude of the miracle.
If the Magnitude of the miracle is higher than two, the consecrating team must have at least one Principal.

The wielder must be a member of either the organization that created the blessed item or an allied organization.
Blessed items can not be broken by normal non-magical means.

### Create Idol
Magnitude 4

This miracle requires a symbol of the character's organization worth 100 GD to be made, while a Principal reads from the sacred texts of the organization telling the tales and myths.
This stores the organization's miracles, and can be used by Principals, Champions and Supplicants to regain miracles and can be used by Lay Members to receive ‘lessons', via visions, upon touching the idol.
Such visions will increase the Relationship skill of a Lay Member by $$1\text{D}10\%$$ when they are repeatedly exposed to them.

### Dismiss Magic
Ranged, Instant, Progressive, Magnitude 1

Dismiss Magic may be cast against either a general target or a specific dweomer/knack/miracle.
Dismiss Magic will eliminate a combined Magnitude of effects equal to its own Magnitude, starting with the most powerful affecting the target.
If it fails to eliminate any effect (because the effect's Magnitude is too high), then its effects immediately end and no more effects will be eliminated.
An effect cannot be partially eliminated, so a target under the effects of a effect whose Magnitude is higher than that of Dismiss Magic will not have any effect currently affecting it eliminated.
The character can also target Dismiss Magic against a single specific dweomer/knack/miracle.

As long as Dismiss Magic's Magnitude equals or exceeds the target effect's Magnitude, the target effect is countered.

Dismiss Magic may be fired as a Reaction, but only when another miracle/knack/dweomer is used within Dismiss Magic's Range that the character wishes to counter.
A successful Dismiss Magic disrupts the other effect and nullifies it.

### Divination
Instant, Progressive, Magnitude 1

For each point of Magnitude of this miracle the character can ask one question of their Grantor, which they must answer to the best of their ability.
A Grantor can only answer questions it knows about and that falls within its sphere of power.
Grantors will also commonly ask for a small act per question asked.
Particularly cruel and bloodthirsty Grantors will demand blood sacrifices of sentient beings.

### Enhance Fertility
Permanent, Magnitude 3

This miracle makes any one creature more fertile than normal.
If it is an animal that has multiple offspring then it doubles the number.
For creatures who have singular births, it guarantees conception and birth of the offspring.

This miracle negates the effects of any 'Block Fertility' miracle.

### Enhance Unit
Duration 6 hours, Magnitude 3

For the duration of this miracle a troop of up to thirty soldiers gain $$+1\text{D}6$$ damage.
Also they cannot be routed or affected by any mind control magic (such as Befuddle).

### Excommunicate
Permanent, Magnitude 5, Resist (Persistence)

This miracle severs the mystical link a Supplicant enjoys with their organization, causing the target of the miracle to immediately and permanently lose all miracles from the character's cult.
The target will never again be able to acquire or perform miracles from the cult.

This miracle must be performed by a Principal and takes one hour to perform.
The Supplicant need not be present.
The miracle can only be performed on Supplicants or higher and who belong to the same organization as the performer of the miracle.

### Extension
Progressive, Duration Special, Magnitude 1

This miracle lengthens the duration of any other miracle with the Duration trait.
Extension, and the miracle it is extending, are performed simultaneously by the character – this is an exception to the normal rule that only one miracle may be performed during a single Combat Round.

Each point of Magnitude of the Extension miracle doubles the target miracle's duration.
Thus, a Magnitude 1 Extension increases Breathe Water's Duration to 30 minutes, a Magnitude 2 increases it to one hour, Magnitude 3 increases it to two hours, Magnitude 4 increases it to four hours, and so on.

### Fear
Ranged, Instant, Magnitude 1, Resist (Persistence)

This spell causes the target to be gripped with overwhelming fear.
Fear has no effect on unconscious targets, targets without an REA Characteristic or targets that are currently under the influence of a Fear spell.

| Target's Persistence Test Result | Effect |
|:---------------------------------|:-------|
| Fumble                           | Victim instantly loses half original Hit Points in damage (enough to cause a Major Wound,) as its heart stops.
| Fail                             | Victim flees in screaming terror for a number of Combat Rounds equal to 20 minus its CHA (minimum of one Combat Round). Victim will not engage in combat unless forced to and will use the Run Combat Action whenever possible (unless a faster mode of egress is available). 
| Success                          | Victim is shaken and disturbed, suffering a –25% penalty to all skill tests for a number of Combat Rounds equal to 20 minus its CHA (minimum of one Combat Round).
| Critically Success               | Victim is unaffected by the spell and cannot be affected by further Fear spells for a number of Combat Rounds equal to its CHA.

### Find X
Ranged, Duration 15, Magnitude 1

This is actually several miracles, though they all operate in a similar fashion, which allow the character to locate the closest target of the miracle within its range.
This effect is stopped by a thick substance such as metal, earth or stone if it is at least one yard thick.
It is also blocked by Absorption, though the character will know the target is somewhere within range (though not its precise location) and that it is being protected by Absorption.
The separate Find miracles are listed below.

Unlike the Detect knacks, the Find miracles do not require concentration on the part of the character – they simply function and alert them to the presence of whatever they are meant to locate.

+ Find Enemy: Gives the location of one creature who intends to harm the character.
+ Find Magic: Gives the location of the nearest magic item, magical creature, or active miracle.
+ Find (Species): Each Find Species miracle will give the location of the nearest creature of the specified species.
  Examples of this miracle include Find Griffin, Find Deer and Find Eagle
+ Find Substance: Each Find Substance miracle will give the location of the nearest substance of the specified type.
  Examples of this miracle include Find Coal, Find Gold and Find Wood.

The Games Master should provide the rough power of the detected subject (‘weak magic' or ‘rich gold lode').

### Illusion
Ranged, Duration 15, Progressive, Magnitude 1

This miracle creates an illusion based on all five senses.
The illusion will seem real and solid unless the person looking at it succeeds in a Perception test, which is subject to a modifier based on the Magnitude of the miracle.
If the viewer succeeds in a Perception test and the Illusion could usually cause damage if believed in, it can no longer cause damage to that character.
As soon as a viewer disbelieves the illusion it becomes insubstantial and ghost like to them.

The Size of the illusion is also governed by the Magnitude.
A Magnitude 1 Illusion can quite happily create small household items, say a fake table and chair, but would not be able to create an illusion of a fire breathing Dragon.

| Magnitude | Perception Test Modifier | Type of Illusion Possible |
|:---------:|:------------------------:|:--------------------------|
| 1         | +50%                     | Not capable of motion or causing damage. Slightly fuzzy and unreal round the edges. No larger than 5 cubic yards.
| 2         | +25%                     | Some minor discrepancies. Capable of motion, but not of damage. No larger than 10 cubic yards.
| 3         | -                        | Capable of motion and causing damage. No larger than 15 cubic yards.
| 4         | -25%                     | Capable of motion and causing damage. No larger than 20 cubic yards.
| 5         | -50%                     | Indistinguishable from the real thing, capable of motion and damage. No larger than 25 cubic yards.
| +1        | -50%                     | +5 cubic yards to size per Magnitude

### Jigsaw
Duration 6 hours, Magnitude 4

This miracle literally causes the target to fall to pieces upon a failed Persistence roll.
The victim will still be alive, but will not require food or water during the time the miracle is in operation.
If the miracle is dispelled the victim will die unless they have been painstakingly put together beforehand.

### Lightning Strike
Ranged, Instant, Progressive, Magnitude 1, Resist (Dodge)

This miracle causes a sizzling bolt of lightning to streak from the hand of the character toward the target.
If the bolt is not dodged, each point of Magnitude of the miracle will cause $$1\text{D}6$$ damage.
AP are not effective against this damage and it counts as both magical and electrical damage.

### Madness
Ranged, Instant, Magnitude 1, Resist (Persistence)

This miracle causes the target to lose contact with reality and become a gibbering loon.
Madness has no effect on unconscious targets, targets without an REA Characteristic or targets that are currently under the effect of a Madness miracle.

| Target's Persistence Test Result | Effect |
|:--------------------------------:|:-------|
| Fumble                           | Victim instantly loses $$1\text{D}4$$ REA permanently and lapses into a catatonic state for a number of minutes equal to 20 minus its CHA (minimum of one Combat Round).
| Fail                             | Victim gibbers and raves uncontrollably for a number of Combat Rounds equal to 20 minus its CHA (minimum of one Combat Round). Victim will perform random Combat Actions during this period. Roll $$1\text{D}6$$: 1. Move to close combat attack a random target; 2. Run in a random direction; 3. Cast a spell at random target; 4. Use ranged attack against random target; 5. Shout at random target; 6. Change stance.
| Success                          | Victim is shaken and disturbed, suffering a –25% penalty to all skill tests for a number of Combat Rounds equal to 20 minus its CHA (minimum of one Combat Round).
| Critical Success                 | Victim is unaffected by the miracle and cannot be affected by further Madness miracles for a number of Combat Rounds equal to its CHA.

### Mindblast
Ranged, Instant, Progressive, Magnitude 1, Resist (Persistence)

This miracle applies a penalty to the victim's REA equal to the Magnitude of the miracle.
The effect lasts a number of days equal to the character's current CHA.

### Mindlink
Ranged Duration 15, Progressive, Magnitude 1

This miracle allows the transmission of conscious thoughts, knowledge, Power Points between participants.
Additional points of Magnitude allow multiple sets of people to be linked together, either creating several separate pairs of Mindlinked people, or making the character the central hub of a small Mindlink network.
In the second case, only the ‘central' character is linked directly to other participants.

Mindlink must be performed upon all participants at the same time and is limited to consenting participants.
Any participant in a Mindlink may use the miracle knowledge and Power Points of others they are linked to without consent.

Participants in a Mindlink have a special vulnerability to INT, REA, CHA and morale-affecting miracles.
Such a miracle performed against any member of a Mindlink will affect all those connected, though all participants are entitled to defend themselves individually.

Although participants in a Mindlink share Power Points and conscious thought, they remain their own entity.
Mindlink does not include hidden thoughts, memories, unconscious urges or knacks.
A Mindlink participant may perform a miracle one of the other participants has acquired, but only if they they must pass a Supplication test using their own score – Mindlink does not allow sharing of skills.

Any participant may sever their connection to the Mindlink as a Combat Action.
If any participant in Mindlink leaves the miracle's range, that participant is considered to have left the Mindlink.

### Miraculous Item
Duration 1 Hour, Magnitude Variable

This miracle allows the caster to create items literally from nowhere.
The size of the item depends upon the Magnitude of the miracle.

Such items are always of the finest quality and do not break under any circumstance.
For the cost of the Magnitude in Improvement Points the item can last permanently.
Otherwise it disappears after one hour.

| Magnitude | Size |
|:---------:|:-----|
| 1         | Small items, such as pots, plates, knives, a defaced detail on a stone fresco, etc.
| 2         | Medium. Large containers such as wine amphorae, target shields, longswords, human sized armour, a missing arm on a broken statue.
| 3         | Large. Tower shields, broken doors, a missing masonry feature such as a column.
| 4         | Huge. Giant armour, ruined houses, shattered towers.
| 5         | Ginormous. The broken parts of a walking castle, the ruined walls of a city.

### Puppet
Duration 6 hours, Concentration, Magnitude 3, Resist (Persistence)

This in its base form, is mind control.
The character uses this miracle to enchant a puppet which is the focus of the miracle and the stand-in for the victim.
The victim can use Persistence to resist the miracle, and if they fail their actions are controlled via the puppet by the character, for the duration of the miracle.
The victim is aware of not being in control of their body, but is powerless to stop it.
The exception to this rule is when the victim's life, or that of a friend is in danger because of the Puppeteer's commands.
Then the victim is allowed another Persistence roll, and, if successful, breaks free of the miracle's control.

### Quicksand
Area, Magnitude 2

This miracle creates a patch of quicksand 10 yards square.
The earth in the area becomes boggy and saturated with water.
On a failed Persistence roll, any character standing on the patch will sink down into the earth, at a rate of 1 yard a round, the quicksand holds the victim firmly and only by dispelling the miracle can they be rescued.

Once the duration of the miracle is over the victim, if not on the surface, is buried under the now solid earth and suffocating.

### Radiant Appearance
Duration 1 Day, Magnitude 2

The recipient of this spell glows with light and power.
They treat any Influence tests as easy for the duration of the spell.
Also any light-hating creatures suffer -25% when attacking the recipient.

### Reflection
Ranged, Duration 15, Progressive, Magnitude 1

This miracle reflects incoming spells aimed at the target or his equipment, redirecting the dweomer, knack, or miracle back at the original caster.
Once performed on a subject, Reflection will attempt to reflect any effects targeting them.
It will not have any effect on effects that are already affecting a character.
The effects of Reflection depend on the relative Magnitude of both itself and the incoming effect – see the Reflection Results table for more details.
Reflection is incompatible with Absorption and Shield.

| Incoming Effect's Magnitude Is...           | Effect |
|:-------------------------------------------:|:-------|
| Equal or weaker than reflection's magnitude | Incoming effect reflected and Reflection remains.
| Stronger than reflection's magnitude        | Reflection eliminated and incoming spell takes effect.

### Repair and Replace
Instant, Magnitude Variable

This spell repairs broken crafted items.
It also replaces missing parts of an item.
The size of the item depends on the Magnitude of the spell.

1. Small items, such as pots, plates, knives, a defaced detail on a stone fresco, etc.
2. Medium. Large containers, such as wine amphorae, target shields, longswords, human sized armour, a missing arm on a broken statue.
3. Large. Tower shields, broken doors, a missing masonry feature such as a column.
4. Huge. Giant armour, ruined houses, shattered towers.
5. Ginormous. The broken parts of a walking castle, the ruined walls of a city.

### Rout
Magnitude 3

When aimed at a body of warriors, no more than 100 persons, they make a Persistence roll or immediately lose all cohesion as a unit and rout.
Routing units move at double movement, away from the character to ideally a place of safety.
They will not defend themselves, but will attack any enemy units that get in their way, with the aim of getting through them to their place of safety.

### See Past
Area, Concentration, Magnitude 2

When performed on a 10 yard area, the character as long as they concentrate can see the area, as it was in any past point of time he wishes.
They still need to make successful Perception rolls to notice details, such as important clues, and they can not interact with the scene they see in any way, shape or form.

### Shield
Duration 15, Progressive, Magnitude 1

This miracle protects the character from physical and magical attacks.
Each point of Magnitude gives the character one Armour Point and provides a +10% bonus to any tests the character may make to resist malign magical effects.
A Magnitude 4 Shield miracle provides the character with +4 AP and a +40% bonus against malign magic, for instance.
These effects are cumulative with others, as well as any physical armour the character is wearing.
Shield is incompatible with Absorption and Reflection.

### Soul Sight
Touch, Duration 15, Magnitude 1

This miracle allows the recipient to see the magic aura of anyone he looks at, enabling them to discern that creature's current Power Points, as well as the nature of any active magic effects or enchanted items the creature is carrying.

### Sun Disc
Ranged, Magnitude 1, Resist (Dodge)

Upon performing this miracle, the character projects a disc of blinding light (roll vs Dodge or be blinded for $$1\text{D}4$$ hours) from their hand.
Its warming effect melts ice upon contact, even magical ice if under three Magnitude in power and makes Resistance tests against cold easy for creatures in contact with the disc.

### Sunspear
Ranged, Instant, Magnitude 4, Resist (Dodge)

This miracle will only function in direct sunlight.
When performed, a shaft of light two yards wide streaks from the sky to blast a single target, who must be visible to the character.
If the target does not dive out of the way, the blazing light will burn it for $$4\text{D}6$$ damage.
Armor points are not effective against this damage and it counts as both magical and fire damage.

### Sureshot
Ranged, Duration 15, Magnitude 1

Performed on a missile weapon (such as a knife, arrow, javelin or rock), this miracle is triggered when it is fired.
Unless the wielder of the weapon rolls an automatic failure or a fumble, the missile hits successfully (though it may be dodged or parried).
So long as the target is within the maximum range of the weapon, the missile will strike home, regardless of concealment or any other factors.
Attempts to parry or dodge the missile are hard tests.

Sureshot may not be combined with Firearrow, Multimissile or Speedart – Sureshot will always take precedence in such cases.

### Touch of Death
Touch, Instant, Magnitude 4, Resist (Persistence)

The character must touch their victim and on a failed Persistence test the victim falls down dead.

### Treasury
Duration 1 Day, Magnitude 4

This creates a secure room for one day, to store valuables in.
All the entrances are locked and only the character can come in and out without setting off a magical alarm that they can hear no matter how far away from the room they are.

### True Weapon
Ranged, Duration 15, Magnitude 3

Performed on a close combat weapon, this spell doubles that weapon's normal damage dice.
Other modifiers, such as Damage Modifier, are not affected.
The wielder of the weapon should roll the weapon's damage twice and total the result.

### Ward Camp
Area, Duration 8 hours, Magnitude 2

This miracle protects a camp with  an area of 10 square yards.
Anyone crossing the invisible boundary of the miracle takes $$1\text{D}10$$ damage, and sets off a magical alarm that immediately awakens everyone within the camp.
The Ward stays in place, even after it has been crossed, for the full duration of the spell.

### Wax Effigy
Magnitude 4, Resist (Persistence)

This miracle enchants a small wax representation of the intended victim.
Miracles can be cast at the effigy and affect the victim, despite the distance between the effigy and the victim.
The character need not need have seen/met the victim, since it is the power of their god that is providing the link.
Once a day the victim can be caused physical harm by driving pins into the effigy, at $$1\text{D}4$$ damage per pin.
The character can attempt to kill the victim outright by breaking off the head of the effigy.
In this case the victim gets a Persistence roll to avoid death.
On a failed Persistence test the victim dies.
On a successful Persistence roll the effigy no longer has any power over the victim.

### Whirlwind
Duration 15, Variable

Each point of Magnitude of this spell whips up a whirlwind capable of carrying a medium creature in its whirling vortex.
Each round the Whirlwind moves ten yards per point, in a random direction (use a $$\text{D}8$$ to determine direction, with 1 being North and 5 being South, progressing clockwise round the directions).
For each point of Magnitude, the Whirlwind is 10 yards tall.

If a character is hit by the Whirlwind, make a Dodge roll to avoid being caught up in it.
Characters who are caught are whipped off their feet, $$\text{D}6$$ yards into the vortex.
Each round roll a $$\text{D}6$$.

| Roll $$\text{D}6$$ | Result |
|:-------:|:-------|
| 1-2     | Carried up $$\text{D}10$$ yards (if already at the top, blown out the whirlwind the additional height before falliing to earth (taking damage).
| 3-4     | Stay at the height they are
| 5-6     | Fall $$\text{D}6$$ down in the vortex. If this takes them to the ground they take falling damage.