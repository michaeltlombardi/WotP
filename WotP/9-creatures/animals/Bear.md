### Bear
> **[danger] Bear**
>
> #### Characteristics:
> **BOD:** $$3\text{D}6 + 12 (23)$$ **DEX:** $$3\text{D}6 (11)$$ **INT:** $$3\text{D}6 (11)$$ **REA:** $$5$$ **CHA:** $$3\text{D}6 (11)$$
> #### Attributes:
> **HP:** $$17$$ **MW:** $$9$$ **AP:** $$3$$ **MR:** $$11$$
> #### Combat:
> + **Close Combat:** $$60\%$$
>   + **Bite:** $$1\text{D}8 + 6$$
>   + **Claw:** $$1\text{D}6 + 6$$