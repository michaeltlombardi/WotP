# Triceratops
> **[danger] Triceratops**
>
> #### Characteristics:
> **BOD:** $$5\text{D}6 + 22 (40)$$ **DEX:** $$2\text{D}6 + 3 (10)$$ **INT:** $$2\text{D}6 (7)$$ **REA:** $$3$$ **CHA:** $$3\text{D}6 (11)$$
> #### Attributes:
> **HP:** $$26$$ **MW:** $$13$$ **AP:** $$10$$ **MR:** $$20$$
> #### Combat:
> + **Close Combat:** $$50\%$$
>   + **Tail Lash:** $$1\text{D}12 + 10$$
>   + **Gore:** $$1\text{D}10 + 10$$