# Cattle
> **[danger] Cattle**
>
> #### Characteristics:
> **BOD:** $$3\text{D}6 + 13 (24)$$ **DEX:** $$2\text{D}6 (7)$$ **INT:** $$2\text{D}6 (7)$$ **REA:** $$2$$ **CHA:** $$2\text{D}6 (7)$$
> #### Attributes:
> **HP:** $$16$$ **MW:** $$8$$ **AP:** $$2$$ **MR:** $$14$$
> #### Combat:
> + **Close Combat:** $$40\%$$
>   + **Charge:** $$1\text{D}8 + 6$$
>   + **Trample:** $$1\text{D}8 + 6$$