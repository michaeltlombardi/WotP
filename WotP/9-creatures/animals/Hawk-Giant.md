# Hawk, Giant
> **[danger] Hawk, Giant**
>
> #### Characteristics:
> **BOD:** $$6\text{D}6 + 16 (37)$$ **DEX:** $$3\text{D}6 + 9 (18)$$ **INT:** $$2\text{D}6 +12 (19)$$ **REA:** $$4$$ **CHA:** $$3\text{D}6 (11)$$
> #### Attributes:
> **HP:** $$24$$ **MW:** $$12$$ **AP:** $$3$$ **MR:** $$9 (\text{land}), 18 (\text{flying})$$
> #### Combat:
> + **Close Combat:** $$80\%$$
>   + **Claw:** $$1\text{D}8 + 9$$
>   + **Bite:** $$1\text{D}6 + 9$$