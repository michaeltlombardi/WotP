# Horse
> **[danger] Horse**
>
> #### Characteristics:
> **BOD:** $$2\text{D}6 + 14 (21)$$ **DEX:** $$2\text{D}6+12 (19)$$ **INT:** $$3\text{D}6 (11)$$ **REA:** $$4$$ **CHA:** $$3\text{D}6 (11)$$
> #### Attributes:
> **HP:** $$17$$ **MW:** $$9$$ **AP:** $$2$$ **MR:** $$38$$
> #### Combat:
> + **Close Combat:** $$40\%$$
>   + **Kick:** $$1\text{D}6 + 5$$