# Crocodile, Giant
> **[danger] Crocodile, Giant**
>
> #### Characteristics:
> **BOD:** $$4\text{D}6 + 8 (22)$$ **DEX:** $$3\text{D}6 (11)$$ **INT:** $$2\text{D}6 (7)$$ **REA:** $$2$$ **CHA:** $$3\text{D}6 (11)$$
> #### Attributes:
> **HP:** $$17$$ **MW:** $$9$$ **AP:** $$5$$ **MR:** $$11 (\text{land}) / 6 (\text{water})$$
> #### Combat:
> + **Close Combat:** $$50\%$$
>   + **Attack:** $$1\text{D}8 + 6$$