# Python, Giant
> **[danger] Python, Giant**
>
> #### Characteristics:
> **BOD:** $$3\text{D}6 + 8 (19)$$ **DEX:** $$2\text{D}6 + 6 (13)$$ **INT:** $$2\text{D}6 (7)$$ **REA:** $$3$$ **CHA:** $$3\text{D}5 (11)$$
> #### Attributes:
> **HP:** $$15$$ **MW:** $$8$$ **AP:** $$3$$ **MR:** $$13$$
> #### Combat:
> + **Close Combat:** $$50\%$$
>   + **Bite:** $$1\text{D}4 + 5$$
>   + **Constrict:** $$1\text{D}8 + 5$$