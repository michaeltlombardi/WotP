# Velociraptor
> **[danger] Velociraptor**
>
> #### Characteristics:
> **BOD:** $$3\text{D}6 + 7 (18)$$ **DEX:** $$4\text{D}6 (14)$$ **INT:** $$3\text{D}6 (11)$$ **REA:** $$4$$ **CHA:** $$2\text{D}6 + 6 (13)$$
> #### Attributes:
> **HP:** $$15$$ **MW:** $$8$$ **AP:** $$5$$ **MR:** $$28$$
> #### Combat:
> + **Close Combat:** $$50\%$$
>   + **Bite:** $$1\text{D}8 + 5$$
>   + **Claw:** $$1\text{D}6 + 5$$
>   + **Foreclaw:** $$1\text{D}4 + 5$$