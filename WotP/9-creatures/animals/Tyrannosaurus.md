# Tyrannosaurus
> **[danger] Tyrannosaurus**
>
> #### Characteristics:
> **BOD:** $$4\text{D}6 + 33 (47)$$ **DEX:** $$2\text{D}6 + 3 (10)$$ **INT:** $$2\text{D}6 (7)$$ **REA:** $$3$$ **CHA:** $$2\text{D}6 + 6 (13)$$
> #### Attributes:
> **HP:** $$30$$ **MW:** $$15$$ **AP:** $$10$$ **MR:** $$10$$
> #### Combat:
> + **Close Combat:** $$60\%$$
>   + **Bite:** $$1\text{D}10 + 12$$
>   + **Stomp:** $$1\text{D}10 + 12$$