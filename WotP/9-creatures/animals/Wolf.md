# Wolf
> **[danger] Wolf**
> #### Characteristics:
> **BOD:** $$3\text{D}6 + 2 (13)$$ **DEX:** $$3\text{D}6 + 2 (13)$$ **INT:** $$2\text{D}6 + 12 (19)$$ **REA:** $$5$$ **CHA:** $$3\text{D}6 (11)$$
> #### Attributes:
> **HP:** $$12$$ **MW:** $$6$$ **AP:** $$1$$ **MR:** $$26$$
> #### Combat:
> + **Close Combat:** $$50\%$$
>   + **Bite:** $$1\text{D}8 + 3$$
>   + **Claw:** $$1\text{D}6 + 3$$