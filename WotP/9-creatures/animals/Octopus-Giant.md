# Octopus, Giant
> **[danger] Octopus, Giant**
>
> #### Characteristics:
> **BOD:** $$9\text{D}6 + 2 (34)$$ **DEX:** $$3\text{D}6 + 12 (23)$$ **INT:** $$3\text{D}6 (11)$$ **REA:** $$4$$ **CHA:** $$3\text{D}6 (11)$$
> #### Attributes:
> **HP:** $$23$$ **MW:** $$12$$ **AP:** $$4$$ **MR:** $$12 (\text{land}) / 23 (\text{water})$$
> #### Combat:
> + **Close Combat:** $$50\%$$
>   + **Bite:** $$1\text{D}8 + 9$$
>   + **Arm:** $$1\text{D}4 + 9$$