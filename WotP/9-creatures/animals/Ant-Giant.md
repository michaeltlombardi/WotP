### Ant, Giant
> **[danger] Ant, Giant**
>
> #### Characteristics:
> **BOD:** $$3\text{D}6 + 2 (13)$$ **DEX:** $$2\text{D}5 + 6 (13)$$ **INT:** $$1\text{D}6 (3)$$ **REA:** $$2$$ **CHA:** $$1\text{D}6 + 3 (7)$$
> #### Attributes:
> **HP:** $$10$$ **MW:** $$5$$ **AP:** $$5$$ **MR:** $$13$$
> #### Combat:
> + **Close Combat:** $$60\%$$
>   + **Bite:** $$1\text{D}8 + 3$$