# Crab, Giant
> **[danger] Crab, Giant**
>
> #### Characteristics:
> **BOD:** $$3\text{D}6 + 18 (29)$$ **DEX:** $$2\text{D}6 (7)$$ **INT:** $$1\text{D}6 (4)$$ **REA:** $$2$$ **CHA:** $$3\text{D}6 (11)$$
> #### Attributes:
> **HP:** $$20$$ **MW:** $$10$$ **AP:** $$6$$ **MR:** $$7 (\text{land}) / 4 (\text{water})$$
> #### Combat:
> + **Close Combat:** $$50\%$$
>   + **Claw:** $$1\text{D}10 + 7$$