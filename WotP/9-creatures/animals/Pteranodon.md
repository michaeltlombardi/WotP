# Pteranodon
> **[danger] Pteranodon**
>
> #### Characteristics:
> **BOD:** $$3\text{D}6 + 7 (18)$$ **DEX:** $$2\text{D}6 + 12 (19)$$ **INT:** $$2\text{D}6 (7)$$ **REA:** $$3$$ **CHA:** $$3\text{D}6 (11)$$
> #### Attributes:
> **HP:** $$15$$ **MW:** $$8$$ **AP:** $$3$$ **MR:** $$10 (\text{land}) / 19 (\text{air})$$
> #### Combat:
> + **Close Combat:** $$50\%$$
>   + **Bite:** $$1\text{D}8 + 5$$
>   + **Claw:** $$1\text{D}6 + 5$$