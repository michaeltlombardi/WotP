# Elephant
> **[danger] Elephant**
>
> #### Characteristics:
> **BOD:** $$5\text{D}6 + 23 (41)$$ **DEX:** $$3\text{D}6 (11)$$ **INT:** $$3\text{D}6 (11)$$ **REA:** $$5$$ **CHA:** $$2\text{D}6 + 6 (13)$$
> #### Attributes:
> **HP:** $$27$$ **MW:** $$14$$ **AP:** $$3$$ **MR:** $$11$$
> #### Combat:
> + **Close Combat:** $$45\%$$
>   + **Trample:** $$1\text{D}12 + 10$$
>   + **Tusk:** $$1\text{D}10 + 10$$
>   + **Trunk:** $$1D4$$, Grapple