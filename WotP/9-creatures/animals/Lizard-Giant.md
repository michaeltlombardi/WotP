# Lizard, Giant
> **[danger] Lizard, Giant**
>
> #### Characteristics:
> **BOD:** $$2\text{D}6 + 8 (15)$$ **DEX:** $$1\text{D}6 + 12 (19)$$ **INT:** $$2\text{D}6 (7)$$ **REA:** $$3$$ **CHA:** $$3\text{D}6 (11)$$
> #### Attributes:
> **HP:** $$13$$ **MW:** $$7$$ **AP:** $$2$$ **MR:** $$19$$
> #### Combat:
> + **Close Combat:** $$25\%$$
>   + **Bite:** $$1\text{D}6 + 4$$
>   + **Kick:** $$1\text{D}8 + 4$$