# Hawk
> **[danger] Hawk**
>
> #### Characteristics:
> **BOD:** $$1\text{D}4 (3)$$ **DEX:** $$3\text{D}6 + 18 (29)$$ **INT:** $$2\text{D}6 + 12 (19)$$ **REA:** $$4$$ **CHA:** $$2\text{D}6 (7)$$
> #### Attributes:
> **HP:** $$5$$ **MW:** $$3$$ **AP:** $$0$$ **MR:** $$15 (\text{walking}) / 29 (\text{flying})$$
> #### Combat:
> + **Close Combat:** $$50\%$$
>   + **Claw:** $$1\text{D}6 + 1$$
>   + **Bite:** $$1\text{D}4 + 1$$